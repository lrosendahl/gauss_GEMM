all: gemm_tbb_v5a_sw03_gcc gemm_tbb_v5a_sw03_icc

naive: gemm_naive_gcc gemm_naive_icc

conv_runner:
	g++ src/ConvRunner.cpp -o build/conv-runner -ltbb -std=c++11 -g -O3

benchmarks:
	g++ src/Benchmarks.cpp -o build/benchmarks -ltbb -lmkl_rt -lippi -lipps -lippcore -lippcv -std=c++11 -g -O3 -mavx

gauss_benchmark:
	g++ src/GaussBenchmark.cpp -o build/gauss_benchmark -ltbb -lippi -lipps -lippcore -lippcv -std=c++11 -g -O3 -mavx

gauss_tbb_ipp_benchmark:
	g++ src/GaussBenchmarkTBBIPP.cpp -o build/gauss_tbb_ipp_benchmark -ltbb -lippi -lipps -lippcore -lippcv -std=c++11 -g -O3 -mavx

gauss_tbb_conv_benchmark:
	g++ src/GaussBenchmarkTBBConv.cpp -o build/gauss_tbb_conv_benchmark -ltbb -std=c++11 -g -O3 -mavx -DNUM_WARMUPS=3 -DNUM_EVALUATIONS=10

tests:
	g++ src/Tests.cpp -o build/tests -std=c++11 -ltbb -lmkl -lippi -lipps -lippcore -lippcv -lboost_unit_test_framework -std=c++11 -g -O3 -mavx

sparse_matrix_test:
	g++ src/SparseMatrixTest.cpp -o build/sparse_matrix_test -std=c++11 -lboost_unit_test_framework -g

gemm_tbb_v3:
	g++ src/GemmTBBv3Runner.cpp -o build/gemm_tbb_v3 -std=c++11 -ltbb -O3 -mavx

gemm_tbb_v4:
	g++ src/GemmTBBv4Runner.cpp -o build/gemm_tbb_v4 -std=c++11 -ltbb -lmkl -O3 -mavx

gemm_tbb_v5a:
	g++ src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a -std=c++11 -ltbb -lmkl_rt -O3 -mavx

gemm_tbb_v5c:
	g++ src/GemmTBBv5cRunner.cpp -o build/gemm_tbb_v5c -std=c++11 -ltbb -lmkl_rt -O3 -mavx

tests_v5a:
	g++ src/gemm/GemmTBBv5aTests.cpp -o build/tests-v5a -std=c++11 -ltbb -lmkl -lboost_unit_test_framework -std=c++11 -g -O3 -mavx

gemm_mkl:
	g++ src/GemmMKLRunner.cpp -o build/gemm_mkl -ltbb -lmkl_rt -std=c++11 -g -O3 -mavx

gemm_tbb_v5a_sw03_gcc:
	# g++ src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a_sw03_gcc -std=c++11 -ltbb -O3 -mavx -g -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=100 -DM_L3_CB_SIZE=128 -DM_L2_CB_SIZE=8 -DM_L1_CB_SIZE=1 -DN_L3_CB_SIZE=1024 -DN_L2_CB_SIZE=1024 -DN_L1_CB_SIZE=1024 -DK_L3_CB_SIZE=256 -DK_L2_CB_SIZE=4 -DK_L1_CB_SIZE=4 -DL3_LOOP_ORDER=6 -DL2_LOOP_ORDER=2 -DL1_LOOP_ORDER=5
	g++ src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a_sw03_gcc -std=c++11 -ltbb -O3 -mavx -g -DNUM_WARMUPS=5 -DNUM_EVALUATIONS=25

gemm_tbb_v5a_sw03_icc:
	# icc src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a_sw03_icc -std=c++11 -ltbb -O3 -mavx -g -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=100 -DM_L3_CB_SIZE=32 -DM_L2_CB_SIZE=32 -DM_L1_CB_SIZE=1 -DN_L3_CB_SIZE=16 -DN_L2_CB_SIZE=4 -DN_L1_CB_SIZE=4 -DK_L3_CB_SIZE=512 -DK_L2_CB_SIZE=32 -DK_L1_CB_SIZE=16 -DL3_LOOP_ORDER=1 -DL2_LOOP_ORDER=5 -DL1_LOOP_ORDER=6
	# icc src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a_sw03_icc -std=c++11 -ltbb -O3 -g -march=native -mtune=native -mavx -xhost -O3 -fno-alias -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=100 -DK_L1_CB_SIZE=64 -DK_L2_CB_SIZE=128 -DK_L3_CB_SIZE=1024 -DL1_LOOP_ORDER=6 -DL2_LOOP_ORDER=1 -DL3_LOOP_ORDER=3 -DM_L1_CB_SIZE=1 -DM_L2_CB_SIZE=32  -DM_L3_CB_SIZE=32 -DN_L1_CB_SIZE=16 -DN_L2_CB_SIZE=512  -DN_L3_CB_SIZE=1024
	# icc src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a_sw03_icc -std=c++11 -ltbb -O3 -g -foptimize-sibling-calls -fno-builtin -scalar-rep -ip -mx87 -qopt-mem-layout-trans1 -fomit-frame-pointer -vec-threshold=30 -qopt-block-factor60 -mno-omit-leaf-frame-pointer -unroll54 -fno-jump-tables -fno-alias -no-unroll-aggressive -fdata-sections -qoverride-limits -qopt-malloc-options2 -simd-function-pointers -O3 -m64 -qopt-calloc -fasynchronous-unwind-tables -qopt-prefetch4 -march=native -fexceptions -nolib-inline -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=100 -DK_L1_CB_SIZE=64 -DK_L2_CB_SIZE=128 -DK_L3_CB_SIZE=1024 -DL1_LOOP_ORDER=6 -DL2_LOOP_ORDER=1 -DL3_LOOP_ORDER=3 -DM_L1_CB_SIZE=1 -DM_L2_CB_SIZE=32 -DM_L3_CB_SIZE=32 -DN_L1_CB_SIZE=16 -DN_L2_CB_SIZE=512 -DN_L3_CB_SIZE=1024

gemm_tbb_v5a_m01_icc:
	icc src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a_sw03_icc -std=c++11 -ltbb -O3 -xhost -g -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=100 -DK_L1_CB_SIZE=64 -DK_L2_CB_SIZE=64 -DK_L3_CB_SIZE=512 -DL1_LOOP_ORDER=4 -DL2_LOOP_ORDER=5 -DL3_LOOP_ORDER=6 -DM_L1_CB_SIZE=8 -DM_L2_CB_SIZE=16 -DM_L3_CB_SIZE=32 -DN_L1_CB_SIZE=16 -DN_L2_CB_SIZE=16 -DN_L3_CB_SIZE=32

gemm_no_flags:
	icc src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a_sw03_icc -std=c++11 -ltbb -O3 -g -mavx -xhost -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=100 -DK_L1_CB_SIZE=64 -DK_L2_CB_SIZE=128 -DK_L3_CB_SIZE=1024 -DL1_LOOP_ORDER=6 -DL2_LOOP_ORDER=1 -DL3_LOOP_ORDER=3 -DM_L1_CB_SIZE=1 -DM_L2_CB_SIZE=32  -DM_L3_CB_SIZE=32 -DN_L1_CB_SIZE=16 -DN_L2_CB_SIZE=512  -DN_L3_CB_SIZE=1024

gemm_flags:
	icc src/GemmTBBv5aRunner.cpp -o build/gemm_tbb_v5a_sw03_icc -std=c++11 -ltbb -O3 -g -march=native -mtune=native -mavx -xhost -fno-alias -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=100 -DK_L1_CB_SIZE=64 -DK_L2_CB_SIZE=128 -DK_L3_CB_SIZE=1024 -DL1_LOOP_ORDER=6 -DL2_LOOP_ORDER=1 -DL3_LOOP_ORDER=3 -DM_L1_CB_SIZE=1 -DM_L2_CB_SIZE=32  -DM_L3_CB_SIZE=32 -DN_L1_CB_SIZE=16 -DN_L2_CB_SIZE=512  -DN_L3_CB_SIZE=1024


gemm_naive_gcc:
	g++ src/GemmNaiveRunner.cpp -o build/gemm_naive_gcc -std=c++11 -O3 -mavx -g -DNUM_WARMUPS=3 -DNUM_EVALUATIONS=10

gemm_naive_icc:
	icc src/GemmNaiveRunner.cpp -o build/gemm_naive_icc -std=c++11 -O3 -mavx -g -DNUM_WARMUPS=3 -DNUM_EVALUATIONS=10

spmm_spden_tbb:
	icc src/SparseDenseTBBRunner.cpp -o build/spmm_spden_tbb -std=c++11 -g -ltbb -O3 -mavx -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=25

spmm_spsp_tbb:
	icc src/SparseSparseTBBRunner.cpp -o build/spmm_spsp_tbb -std=c++11 -g -ltbb -O3 -mavx -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=25

spmm_spden_mkl:
	icc src/SparseDenseMKLRunner.cpp -o build/spmm_spden_mkl -std=c++11 -g -mkl -O3 -xhost -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=25

spmm_spsp_mkl:
	icc src/SparseSparseMKLRunner.cpp -o build/spmm_spsp_mkl -std=c++11 -g -mkl -O3 -xhost -DNUM_WARMUPS=10 -DNUM_EVALUATIONS=25

spmm: spmm_spden_mkl spmm_spden_tbb spmm_spsp_mkl spmm_spsp_tbb
