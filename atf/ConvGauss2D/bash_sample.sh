#!/bin/bash

# use the parameter names here
echo "Bash script called with TP_gsx=$TP_gsx and TP_gsy=$TP_gsy"

export GRAIN_SIZE_X="$TP_gsx"
export GRAINS_SIZE_Y="$TP_gsy"

output=$(
  ./conv-runner gauss2D parallel 3000
)

if [ $? -eq 0 ]
then
	# write cost of configuration into cost file
	echo "valid"
	echo "$output" > runtime
    exit 0
else
	#returned with error -> not a valid config
    echo "not valid"
    exit 1
fi
