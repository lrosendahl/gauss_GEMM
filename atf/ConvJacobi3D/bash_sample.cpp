#include "atf.h"

int main(int argc, char *argv[]) {
  auto TP_mc = atf::tp("TP_gsx", atf::interval(64, 1024, 64));
  auto TP_kc = atf::tp("TP_gsy", atf::interval(64, 1024, 64));

  auto tuner = atf::exhaustive();
  tuner(TP_mc, TP_kc);
  auto cf = atf::cf::bash("../bash_sample.sh", "runtime");
  auto best_configuration = tuner(cf);
}
