#include "Benchmarkable.cpp"
#include "GemmTBBv3Const.cpp"
#include "tbb/tick_count.h"

float initValues(int i, int j) { return i * j + 1; }

float time(Benchmarkable &benchmark) {
  tbb::tick_count start = tbb::tick_count::now();
  benchmark.run();
  return (tbb::tick_count::now() - start).seconds();
}


int main(){
	gemmTBBv3Const::GemmTBBv3Const gemmTBBv3Const(initValues, initValues);
        printf("%f", time(gemmTBBv3Const)*1000);
}