#include "atf.h"

int main( int argc, char* argv[] )
{
  auto TP_mc = atf::tp("TP_mc", atf::interval(250,256));
  auto TP_kc = atf::tp("TP_kc", atf::interval(125,128));

  auto tuner = atf::exhaustive();
  tuner(TP_mc, TP_kc);
  auto cf = atf::cf::bash("../bash_sample.sh", "runtime");
  auto best_configuration = tuner(cf);
}
