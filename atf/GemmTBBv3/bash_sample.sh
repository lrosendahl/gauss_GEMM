#!/bin/bash


# use the parameter names here
echo "Bash script called with TP_mc=$TP_mc and TP_kc=$TP_kc"

g++ ATFGemmTBBv3.cpp -O3 -mavx -ltbb -o execute.o -DMC=$TP_mc -DKC=$TP_kc -w

output=$(
	./execute.o 
)

if [ $? -eq 0 ]
then
	# write cost of configuration into cost file 
	echo "valid"
	echo "$output" > runtime
    exit 0	
else
	#returned with error -> not a valid config
    echo "not valid"
    exit 1
fi