#include "atf.h"

int main(int argc, char *argv[]) {
  auto M_L2_CB_SIZE =
      atf::tp("M_L2_CB_SIZE", atf::interval(64, 256, 64), atf::divides(M));
  auto M_L1_CB_SIZE = atf::tp("M_L1_CB_SIZE", atf::interval(64, 256, 64),
                              atf::divides(M_L2_CB_SIZE));

  auto N_L2_CB_SIZE =
      atf::tp("N_L2_CB_SIZE", atf::interval(4, 32, 4), atf::divides(N));
  auto N_L1_CB_SIZE = atf::tp("N_L1_CB_SIZE", atf::interval(4, 32, 4),
                              atf::divides(N_L2_CB_SIZE));

  auto K_L2_CB_SIZE =
      atf::tp("K_L2_CB_SIZE", atf::interval(16, 64, 16), atf::divides(K));
  auto K_L1_CB_SIZE = atf::tp("K_L1_CB_SIZE", atf::interval(16, 64, 16),
                              atf::divides(K_L2_CB_SIZE));

  auto L2_LOOP_ORDER = atf::tp("L2_LOOP_ORDER", atf::interval(1, 1));
  auto L1_LOOP_ORDER = atf::tp("L1_LOOP_ORDER", atf::interval(1, 1));

  auto tuner = atf::open_tuner();
  tuner(G(M_L2_CB_SIZE, M_L1_CB_SIZE), G(N_L2_CB_SIZE, N_L1_CB_SIZE),
        G(K_L2_CB_SIZE, K_L1_CB_SIZE), G(L2_LOOP_ORDER), G(L1_LOOP_ORDER));
  auto cf = atf::cf::bash("../bash_sample.sh", "runtime");
  auto best_configuration = tuner(cf);
}
