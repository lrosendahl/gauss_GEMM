#!/bin/bash

g++ GemmTBBv4Runner.cpp -o gemm_tbb_v4 -std=c++11 -ltbb -O3 -mavx -DM_L2_CB_SIZE=$M_L2_CB_SIZE -DM_L1_CB_SIZE=$M_L1_CB_SIZE -DN_L2_CB_SIZE=$N_L2_CB_SIZE -DN_L1_CB_SIZE=$N_L1_CB_SIZE -DK_L2_CB_SIZE=$K_L2_CB_SIZE -DK_L1_CB_SIZE=$K_L1_CB_SIZE -DL2_LOOP_ORDER=$L2_LOOP_ORDER -DL1_LOOP_ORDER=$L1_LOOP_ORDER

output=$(
	./gemm_tbb_v4
)

if [ $? -eq 0 ]
then
	# write cost of configuration into cost file
	echo "valid, cost=$output"
	echo "$output" > runtime
    exit 0
else
	#returned with error -> not a valid config
    echo "not valid"
    exit 1
fi
