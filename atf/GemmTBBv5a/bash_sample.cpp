#include "atf.h"
#include <chrono>

int main(int argc, char *argv[]) {
  auto M_L3_CB_SIZE =
      atf::tp("M_L3_CB_SIZE", atf::interval(1, 1024), atf::divides(1024));
  auto M_L2_CB_SIZE = atf::tp("M_L2_CB_SIZE", atf::interval(1, 1024),
                              atf::divides(M_L3_CB_SIZE));
  auto M_L1_CB_SIZE = atf::tp("M_L1_CB_SIZE", atf::interval(1, 1024),
                              atf::divides(M_L2_CB_SIZE));

  auto N_L3_CB_SIZE =
      atf::tp("N_L3_CB_SIZE", atf::interval(1, 1024), atf::divides(1024));
  auto N_L2_CB_SIZE = atf::tp("N_L2_CB_SIZE", atf::interval(1, 1024),
                              atf::divides(N_L3_CB_SIZE));
  auto N_L1_CB_SIZE = atf::tp("N_L1_CB_SIZE", atf::interval(1, 1024),
                              atf::divides(N_L2_CB_SIZE));

  auto K_L3_CB_SIZE =
      atf::tp("K_L3_CB_SIZE", atf::interval(1, 1024), atf::divides(1024));
  auto K_L2_CB_SIZE = atf::tp("K_L2_CB_SIZE", atf::interval(1, 1024),
                              atf::divides(K_L3_CB_SIZE));
  auto K_L1_CB_SIZE = atf::tp("K_L1_CB_SIZE", atf::interval(1, 1024),
                              atf::divides(K_L2_CB_SIZE));

  auto L3_LOOP_ORDER = atf::tp("L3_LOOP_ORDER", atf::interval(1, 6));
  auto L2_LOOP_ORDER = atf::tp("L2_LOOP_ORDER", atf::interval(1, 6));
  auto L1_LOOP_ORDER = atf::tp("L1_LOOP_ORDER", atf::interval(1, 6));

  auto tuner = atf::open_tuner(atf::cond::duration<std::chrono::minutes>(10));
  tuner(G(M_L3_CB_SIZE, M_L2_CB_SIZE, M_L1_CB_SIZE),
        G(N_L3_CB_SIZE, N_L2_CB_SIZE, N_L1_CB_SIZE),
        G(K_L3_CB_SIZE, K_L2_CB_SIZE, K_L1_CB_SIZE), G(L3_LOOP_ORDER),
        G(L2_LOOP_ORDER), G(L1_LOOP_ORDER));
  auto cf = atf::cf::bash("../bash_sample.sh", "runtime");
  auto best_configuration = tuner(cf);
}
