#!/bin/bash

# set the number of nodes
#SBATCH --nodes=1

# set the number of CPU cores per node
#SBATCH --ntasks-per-node 64

# set a partition
#SBATCH --partition normal

# set max wallclock time
#SBATCH --time=00:05:00

# set name of job
#SBATCH --job-name=gauss_ipp_job

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=ALL

# set an output file
#SBATCH --output gauss_ipp_output.txt

# send mail to this address
#SBATCH --mail-user=arne.wilp@uni-muenster.de

# run the application
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/lib/ ~/gauss-ipp
