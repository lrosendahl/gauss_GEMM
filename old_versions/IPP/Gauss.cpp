#include "ipp.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <stdio.h>

using namespace std;
using namespace tbb;

int width = 10000;
int height = 10000;

/* Next two defines are created to simplify code reading and understanding */
#define EXIT_MAIN                                                              \
  exitLine: /* Label for Exit */
#define check_sts(st)                                                          \
  if ((st) != ippStsNoErr)                                                     \
    goto exitLine; /* Go to Exit if Intel(R) IPP function returned status      \
                      different from ippStsNoErr */

/* Results of ippMalloc() are not validated because Intel(R) IPP functions
 * perform bad arguments check and will return an appropriate status  */

void run() {
  IppStatus status = ippStsNoErr;
  Ipp32f *pSrc = NULL, *pDst = NULL; /* Pointers to source/destination images */
  int srcStep = 1,
      dstStep = 0; /* Steps, in bytes, through the source/destination images */
  IppiSize roiSize = {width,
                      height}; /* Size of source/destination ROI in pixels */
  Ipp32u kernelSize = 3;
  Ipp32f sigma = 0.391f;
  Ipp8u *pBuffer = NULL;                      /* Pointer to the work buffer */
  IppFilterGaussianSpec *pSpec = NULL;        /* context structure */
  int iTmpBufSize = 0, iSpecSize = 0;         /* Common work buffer size */
  IppiBorderType borderType = ippBorderConst; /*Set constant border values */
  Ipp8u borderValue = 0;
  int numChannels = 1;

  // allocate 64bit aligned memory
  pSrc = ippiMalloc_32f_C1(roiSize.width, roiSize.height, &srcStep);
  pDst = ippiMalloc_32f_C1(roiSize.width, roiSize.height, &dstStep);

  // setup values
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      pSrc[i * srcStep / 4 + j] =
          i * j; /* access of cache aligned entry [i][j]*/
    }
  }

  // Start timer
  // cout << "Starting calculation. " << HEIGHT << " * " << WIDTH << endl;
  tick_count t0 = tick_count::now();
  // setup BufferSize
  ippiFilterGaussianGetBufferSize(roiSize, kernelSize, ipp32f, numChannels,
                                  &iSpecSize, &iTmpBufSize);

  pSpec = (IppFilterGaussianSpec *)ippsMalloc_8u(iSpecSize);
  pBuffer = ippsMalloc_8u(iTmpBufSize);

  // init Gauss kernel with given parameters
  ippiFilterGaussianInit(roiSize, kernelSize, sigma, borderType, ipp32f,
                         numChannels, pSpec, pBuffer);

  // apply Gauss filter
  ippiFilterGaussianBorder_32f_C1R(pSrc, srcStep, pDst, dstStep, roiSize,
                                   borderValue, pSpec, pBuffer);

  // finish timer

  tick_count t1 = tick_count::now();
  printf("size: %i\ttime: %f s\n", width, (t1 - t0).seconds());
  // cout << "Test value: " << pDst[345 * dstStep / 4 + 24] << endl;
  // cout << "Test value: " << pDst[345 * dstStep / 4] << endl;
  // cout << "Test value: " << pDst[24] << endl;
  // cout << "Test value: " << pDst[1234 * dstStep / 4 + 1234] << endl;
  // cout << "Test value: " << pDst[1999 * dstStep / 4 + 1999] << endl;

  // //output result
  //     for(int i = 0; i < HEIGHT; i++){
  //       for(int j = 0; j < WIDTH; j++){
  //         cout << pDst[i*dstStep/4+j] << " ";
  //       }
  //       cout << endl;
  //     }

  EXIT_MAIN
  ippsFree(pBuffer);
  ippsFree(pSpec);
  ippiFree(pSrc);
  ippiFree(pDst);
}

int main() {
  printf("IPP:\n");
  // for (int i = 1000; i <= 25000; i += 1000) {
  //   width = i;
  //   height = i;
  //   run();
  // }
  run();
}
