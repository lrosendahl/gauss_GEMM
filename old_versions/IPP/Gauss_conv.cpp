#include "ipp.h"
#include <iostream>
#include <stdio.h>

#define WIDTH 128 /* image width */
#define HEIGHT 64 /* image height */

float filter[] = {0.00124641, 0.0328117,  0.00124641, 0.0328117, 0.863767,
                  0.0328117,  0.00124641, 0.0328117,  0.00124641};

int main(void) {
  IppStatus status = ippStsNoErr;
  Ipp32f *pSrc = NULL, *pDst = NULL, *pFil = NULL;
  int srcStep = 0, dstStep = 0, filStep = 0;
  IppiSize roiSize = {WIDTH, HEIGHT};
  int bufSize = 0;
  IppiSize filSize = {3, 3};
  Ipp8u *pBuffer = NULL;
  int numChannels = 1;
  IppiROIShape algType = ippiROIValid;

  pSrc = ippiMalloc_32f_C1(roiSize.width, roiSize.height, &srcStep);
  pDst = ippiMalloc_32f_C1(roiSize.width, roiSize.height, &dstStep);
  pFil = ippiMalloc_32f_C1(filSize.width, filSize.height, &filStep);

  status = ippiConvGetBufferSize(roiSize, roiSize, ipp32f, numChannels, algType,
                                 &bufSize);

  pBuffer = ippsMalloc_8u(bufSize);

  std::cout << "srcStep: " << srcStep << std::endl;
  std::cout << "dstStep: " << dstStep << std::endl;
  std::cout << "filStep: " << filStep << std::endl;

  for (int i = 0; i < roiSize.height; i++) {
    for (int j = 0; j < roiSize.width; j++) {
      pSrc[i * srcStep / 4 + j] = i * j;
    }
  }

  for (int i = 0; i < filSize.height; i++) {
    for (int j = 0; j < filSize.width; j++) {
      pSrc[i * filStep / 4 + j] = filter[i * filSize.width + j];
    }
  }

  status = ippiConv_32f_C1R(pSrc, srcStep, roiSize, pFil, filStep, filSize,
                            pDst, dstStep, algType, pBuffer);

  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      printf("%.5f\t", pDst[i * srcStep / 4 + j]);
    }
    printf("\n");
  }

  ippsFree(pBuffer);
  ippiFree(pFil);
  ippiFree(pSrc);
  ippiFree(pDst);
  printf("Exit status %d (%s)\n", (int)status, ippGetStatusString(status));

  return (int)status;
}
