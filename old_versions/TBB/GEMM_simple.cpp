#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <vector>

using namespace tbb;
using namespace std;

const int size = 2048;

double *__restrict__ a;
double *__restrict__ b;
double *__restrict__ formattedB;
double *__restrict__ c;

class Multiply {

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    int rowStart = r.rows().begin();
    int rowEnd = r.rows().end();
    int colStart = r.cols().begin();
    int colEnd = r.cols().end();
    for (size_t i = rowStart; i != rowEnd; ++i) {
      double *cp = &c[i * size + colStart];
      for (size_t j = colStart; j != colEnd; ++j) {
        double sum = 0;
        double *ap = &a[i * size];
        double *bp = &formattedB[j * size];
        for (size_t k = 0; k < size; ++k)
          sum += *(ap++) * *(bp++);
        *(cp++) = sum;
      }
    }
  }
};

class formatB {

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    int rowStart = r.rows().begin();
    int rowEnd = r.rows().end();
    int colStart = r.cols().begin();
    int colEnd = r.cols().end();
    for (size_t i = rowStart; i != rowEnd; ++i) {
      double *bp = &b[i * size + colStart];
      for (size_t j = colStart; j != colEnd; ++j) {
        formattedB[j * size + i] = *(bp++);
      }
    }
  }
};

void printC() {
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      cout << c[i * size + j] << " ";
    }
    cout << endl;
  }
}

int main() {
  a = new double[size * size];
  b = new double[size * size];
  formattedB = new double[size * size];
  c = new double[size * size];

  // Setup a
  for (int i = 0; i < (size); i++) {
    for (int j = 0; j < (size); j++) {
      a[i * size + j] = i * j + 1;
      // a[i*size+j] = 1;
    }
  }

  // Setup b
  for (int i = 0; i < (size); i++) {
    for (int j = 0; j < (size); j++) {
      b[i * size + j] = size * size - i * j - 1;
      // b[i*size+j] = 1;
    }
  }

  cout << "Starting calculation." << endl;

  tick_count t0 = tick_count::now();
  parallel_for(blocked_range2d<size_t>(0, size, 32, 0, size, 32), formatB(), simple_partitioner());
  parallel_for(blocked_range2d<size_t>(0, size, 32, 0, size, 32), Multiply(), simple_partitioner());
  tick_count t1 = tick_count::now();

  cout << "Finished" << endl;
  cout << "Calculation took: " << (t1 - t0).seconds() << " seconds." << endl;
  // printC();
  cout << "Test value: " << c[1234 * size + 456] << endl;

  delete[] a;
  delete[] b;
  delete[] formattedB;
  delete[] c;
}
