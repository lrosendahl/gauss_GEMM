#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include "tbb/task.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <vector>

using namespace std;
using namespace tbb;
const int threshold = 512;
const int matSize = 4096;

// Normal n^3 algorithm for leaves
class Multiply {
  double** __restrict__ my_A;
  double** __restrict__ my_B;
  double** __restrict__ my_result;
  size_t size;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    int rowStart = r.rows().begin();
    int rowEnd = r.rows().end();
    int colStart = r.cols().begin();
    int colEnd = r.cols().end();
    for (size_t i = rowStart; i != rowEnd; ++i) {
      for (size_t j = colStart; j != colEnd; ++j) {
        double sum = 0;
        for (size_t k = 0; k < size; ++k)
          sum += my_A[i][k] * my_B[j][k];
        my_result[i][j] = sum;
      }
    }
  }
  Multiply(double **A, double **B, double **result, size_t s)
      : my_A(A), my_B(B), my_result(result), size(s) {}
};

// STRASSEN Algorithm
// -------------------------------------------------------------- help functions

// add submatrix
class AddMatBlocks {
  double** __restrict__ my_temp;
  double** __restrict__ my_A;
  int my_a1;
  int my_a2;
  size_t size;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    int rowStart = r.rows().begin();
    int rowEnd = r.rows().end();
    int colStart = r.cols().begin();
    int colEnd = r.cols().end();
    for (size_t i = rowStart; i != rowEnd; ++i) {
      for (size_t j = colStart; j != colEnd; ++j) {
        my_temp[i][j] =
            my_A[i + my_a1 * size][j] + my_A[i + my_a2 * size][j + size];
      }
    }
  }
  AddMatBlocks(double **temp, int a1, int a2, double **A, size_t s)
      : my_temp(temp), my_A(A), my_a1(a1), my_a2(a2), size(s) {}
};

// Copy submatrix
class CopyMatBlocks {
  double** __restrict__ my_temp;
  int my_a1;
  double** __restrict__ my_A;
  size_t size;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    int rowStart = r.rows().begin();
    int rowEnd = r.rows().end();
    int colStart = r.cols().begin();
    int colEnd = r.cols().end();
    for (size_t i = rowStart; i != rowEnd; ++i) {
      for (size_t j = colStart; j != colEnd; ++j) {
        my_temp[i][j] = my_A[i + my_a1 * size][j + my_a1 * size];
      }
    }
  }
  CopyMatBlocks(double **temp, int a1, double **A, size_t s)
      : my_temp(temp), my_A(A), my_a1(a1), size(s) {}
};

// Subtract submatrix
class SubMatBlocks {
  double** __restrict__ my_temp;
  int my_a1;
  double** __restrict__ my_A;
  size_t size;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    if (my_a1 == 0) {
      int rowStart = r.rows().begin();
      int rowEnd = r.rows().end();
      int colStart = r.cols().begin();
      int colEnd = r.cols().end();
      for (size_t i = rowStart; i != rowEnd; ++i) {
        for (size_t j = colStart; j != colEnd; ++j) {
          my_temp[i][j] = my_A[i + size][j + my_a1] - my_A[i][j + my_a1];
        }
      }
    } else {
      int rowStart = r.rows().begin();
      int rowEnd = r.rows().end();
      int colStart = r.cols().begin();
      int colEnd = r.cols().end();
      for (size_t i = rowStart; i != rowEnd; ++i) {
        for (size_t j = colStart; j != colEnd; ++j) {
          my_temp[i][j] = my_A[i][j + size] - my_A[i + size][j + size];
        }
      }
    }
  }
  SubMatBlocks(double **temp, int a1, double **A, size_t s)
      : my_temp(temp), my_a1(a1), my_A(A), size(s) {}
};

// combine M1 ... M7
class CombineMatBlocks {
  double** __restrict__ my_M1;
  double** __restrict__ my_M2;
  double** __restrict__ my_M3;
  double** __restrict__ my_M4;
  double** __restrict__ my_M5;
  double** __restrict__ my_M6;
  double** __restrict__ my_M7;
  double** __restrict__ my_result;
  int my_rowPart;
  int my_colPart;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    int rowStart = r.rows().begin();
    int rowEnd = r.rows().end();
    int colStart = r.cols().begin();
    int colEnd = r.cols().end();
    for (size_t i = rowStart; i != rowEnd; ++i) {
      for (size_t j = colStart; j != colEnd; ++j) {
        my_result[i][j] = my_M1[i][j] + my_M4[i][j] - my_M5[i][j] + my_M7[i][j];
        my_result[i][j + my_colPart] = my_M3[i][j] + my_M5[i][j];
        my_result[i + my_rowPart][j] = my_M2[i][j] + my_M4[i][j];
        my_result[i + my_rowPart][j + my_colPart] =
            my_M1[i][j] - my_M2[i][j] + my_M3[i][j] + my_M6[i][j];
      }
    }
  }

  CombineMatBlocks(double **M1, double **M2, double **M3, double **M4, double **M5,
                   double **M6, double **M7, double **result, int rowPart,
                   int colPart)
      : my_M1(M1), my_M2(M2), my_M3(M3), my_M4(M4), my_M5(M5), my_M6(M6),
        my_M7(M7), my_result(result), my_rowPart(rowPart), my_colPart(colPart) {
  }
};

// row major to column major
void formatB(double **formattedB, double **B, size_t size) {

  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      formattedB[i][j] = B[j][i];
    }
  }
}

double **allocateArray(int size) {
  double **array = new double *[size];
  for (int i = 0; i < size; i++)
    array[i] = new double[size];
  return array;
}

//----------------------------------------------------------
// Main algorithm
class strassenMMult : public task {
public:
  // A * B = result
  double **my_A;
  double **my_B;
  double **my_result;
  size_t size;

  // constructor
  strassenMMult(double **A, double **B, double **result, size_t s)
      : my_A(A), my_B(B), my_result(result), size(s) {}

  task *execute() {
    if (size <= threshold) { // optimal value for parallel n^3 algorithm
      double **formattedB = allocateArray(size);
      formatB(formattedB, my_B, size);
      parallel_for(blocked_range2d<size_t>(0, size, 16, 0, size, 32),
                   Multiply(my_A, formattedB, my_result, size),
                   simple_partitioner());
      return NULL;
    }
    // cout << "execute test" << endl;
    int rowPart = size / 2;
    int colPart = size / 2;

    int count = 8; // root + M1 ... M7
    set_ref_count(count);
    // M1 = (A11 + A22)*(B11 + B22)

    // declare temp vectors
    double **tempAM1 = allocateArray(rowPart);
    double **tempBM1 = allocateArray(rowPart);
    double **M1 = allocateArray(rowPart);

    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempAM1, 0, 1, my_A, rowPart),
                 simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempBM1, 0, 1, my_B, rowPart),
                 simple_partitioner());
    strassenMMult &task1 =
        *new (allocate_child()) strassenMMult(tempAM1, tempBM1, M1, rowPart);
    spawn(task1);

    // M2 = (A21 + A22)*B11

    // declare temp vectors
    double **tempAM2 = allocateArray(rowPart);
    double **tempBM2 = allocateArray(rowPart);
    double **M2 = allocateArray(rowPart);

    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempAM2, 1, 1, my_A, rowPart),
                 simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CopyMatBlocks(tempBM2, 0, my_B, rowPart),
                 simple_partitioner());
    strassenMMult &task2 =
        *new (allocate_child()) strassenMMult(tempAM2, tempBM2, M2, rowPart);
    spawn(task2);

    // M3 = A11*(B12 – B22)

    // declare temp vectors
    double **tempAM3 = allocateArray(rowPart);
    double **tempBM3 = allocateArray(rowPart);
    double **M3 = allocateArray(rowPart);

    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CopyMatBlocks(tempAM3, 0, my_A, rowPart),
                 simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 SubMatBlocks(tempBM3, 1, my_B, rowPart), simple_partitioner());
    strassenMMult &task3 =
        *new (allocate_child()) strassenMMult(tempAM3, tempBM3, M3, rowPart);
    spawn(task3);

    // M4 = A22*(B21 – B11)

    // declare temp vectors
    double **tempAM4 = allocateArray(rowPart);
    double **tempBM4 = allocateArray(rowPart);
    double **M4 = allocateArray(rowPart);

    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CopyMatBlocks(tempAM4, 1, my_A, rowPart),
                 simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 SubMatBlocks(tempBM4, 0, my_B, rowPart), simple_partitioner());
    strassenMMult &task4 =
        *new (allocate_child()) strassenMMult(tempAM4, tempBM4, M4, rowPart);
    spawn(task4);

    // M5 = (A11 + A12)*B22

    // declare temp vectors
    double **tempAM5 = allocateArray(rowPart);
    double **tempBM5 = allocateArray(rowPart);
    double **M5 = allocateArray(rowPart);

    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempAM5, 0, 0, my_A, rowPart),
                 simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CopyMatBlocks(tempBM5, 1, my_B, rowPart),
                 simple_partitioner());
    strassenMMult &task5 =
        *new (allocate_child()) strassenMMult(tempAM5, tempBM5, M5, rowPart);
    spawn(task5);

    // M6 = (A21 - A11)*(B11 + B12)

    // declare temp vectors
    double **tempAM6 = allocateArray(rowPart);
    double **tempBM6 = allocateArray(rowPart);
    double **M6 = allocateArray(rowPart);

    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 SubMatBlocks(tempAM6, 0, my_A, rowPart), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempBM6, 0, 0, my_B, rowPart),
                 simple_partitioner());
    strassenMMult &task6 =
        *new (allocate_child()) strassenMMult(tempAM6, tempBM6, M6, rowPart);
    spawn(task6);

    // M7 = (A12 - A22)*(B21 + B22)

    // declare temp vectors
    double **tempAM7 = allocateArray(rowPart);
    double **tempBM7 = allocateArray(rowPart);
    double **M7 = allocateArray(rowPart);

    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 SubMatBlocks(tempAM7, 1, my_A, rowPart), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempBM7, 1, 1, my_B, rowPart),
                 simple_partitioner());
    strassenMMult &task7 =
        *new (allocate_child()) strassenMMult(tempAM7, tempBM7, M7, rowPart);
    spawn_and_wait_for_all(task7);
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CombineMatBlocks(M1, M2, M3, M4, M5, M6, M7, my_result,
                                  rowPart, colPart),
                 simple_partitioner());

    return NULL;
  }
};

int main() {

  double **a = allocateArray(matSize);
  double **b = allocateArray(matSize);
  double **c = allocateArray(matSize);

  // Setup a
  for (int i = 0; i < (matSize); i++) {
    for (int j = 0; j < (matSize); j++) {
      a[i][j] = i * j + 1;
      // a[i][j] = 1;
    }
  }

  // Setup b
  for (int i = 0; i < (matSize); i++) {
    for (int j = 0; j < (matSize); j++) {
      b[i][j] = matSize * matSize - i * j - 1;
      // b[i][j] = 1;
    }
  }

  cout << "Starting calculation." << endl;

  tick_count t0 = tick_count::now();
  strassenMMult &test =
      *new (task::allocate_root()) strassenMMult(a, b, c, matSize);
  task::spawn_root_and_wait(test);
  tick_count t1 = tick_count::now();

  cout << "Finished" << endl;
  cout << "Calculation took: " << (t1 - t0).seconds() << " seconds." << endl;

  cout << "Test value: " << c[1234][456] << endl;

  // for( int i=0; i<matSize; ++i ){
  //   for( int j=0; j<matSize; ++j ){
  //     cout << c[i][j] << " ";
  //   }
  //   cout << endl;
  // }
}
