#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include "tbb/task.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <vector>

using namespace std;
using namespace tbb;
const int threshold = 512;
const int matSize = 2048;

// Normal n^3 algorithm for leaves
class Multiply {
  vector<vector<float>> &my_A;
  vector<vector<float>> &my_B;
  vector<vector<float>> &my_result;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    size_t size = my_A.size();
    for (size_t i = r.rows().begin(); i != r.rows().end(); ++i) {
      for (size_t j = r.cols().begin(); j != r.cols().end(); ++j) {
        float sum = 0;
        for (size_t k = 0; k < size; ++k)
          sum += my_A[i][k] * my_B[j][k];
        my_result[i][j] = sum;
      }
    }
  }
  Multiply(vector<vector<float>> &A, vector<vector<float>> &B,
           vector<vector<float>> &result)
      : my_A(A), my_B(B), my_result(result) {}
};

// STRASSEN Algorithm
// -------------------------------------------------------------- help functions

// add submatrix
class AddMatBlocks {
  vector<vector<float>> &my_temp;
  vector<vector<float>> &my_A;
  int my_a1;
  int my_a2;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    size_t size = my_temp.size();
    for (size_t i = r.rows().begin(); i != r.rows().end(); ++i) {
      for (size_t j = r.cols().begin(); j != r.cols().end(); ++j) {
        my_temp[i][j] =
            my_A[i + my_a1 * size][j] + my_A[i + my_a2 * size][j + size];
      }
    }
  }
  AddMatBlocks(vector<vector<float>> &temp, int a1, int a2,
               vector<vector<float>> &A)
      : my_temp(temp), my_A(A), my_a1(a1), my_a2(a2) {}
};

// Copy submatrix
class CopyMatBlocks {
  vector<vector<float>> &my_temp;
  int my_a1;
  vector<vector<float>> &my_A;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    size_t size = my_temp.size();
    for (size_t i = r.rows().begin(); i != r.rows().end(); ++i) {
      for (size_t j = r.cols().begin(); j != r.cols().end(); ++j) {
        my_temp[i][j] = my_A[i + my_a1 * size][j + my_a1 * size];
      }
    }
  }
  CopyMatBlocks(vector<vector<float>> &temp, int a1, vector<vector<float>> &A)
      : my_temp(temp), my_A(A), my_a1(a1) {}
};

// Subtract submatrix
class SubMatBlocks {
  vector<vector<float>> &my_temp;
  int my_a1;
  vector<vector<float>> &my_A;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    size_t size = my_temp.size();
    if (my_a1 == 0) {
      for (size_t i = r.rows().begin(); i != r.rows().end(); ++i) {
        for (size_t j = r.cols().begin(); j != r.cols().end(); ++j) {
          my_temp[i][j] = my_A[i + size][j + my_a1] - my_A[i][j + my_a1];
        }
      }
    } else {
      for (size_t i = r.rows().begin(); i != r.rows().end(); ++i) {
        for (size_t j = r.cols().begin(); j != r.cols().end(); ++j) {
          my_temp[i][j] = my_A[i][j + size] - my_A[i + size][j + size];
        }
      }
    }
  }
  SubMatBlocks(vector<vector<float>> &temp, int a1, vector<vector<float>> &A)
      : my_temp(temp), my_a1(a1), my_A(A) {}
};

// combine M1 ... M7
class CombineMatBlocks {
  vector<vector<float>> &my_M1;
  vector<vector<float>> &my_M2;
  vector<vector<float>> &my_M3;
  vector<vector<float>> &my_M4;
  vector<vector<float>> &my_M5;
  vector<vector<float>> &my_M6;
  vector<vector<float>> &my_M7;
  vector<vector<float>> &my_result;
  int my_rowPart;
  int my_colPart;

public:
  void operator()(const blocked_range2d<size_t> &r) const {
    for (size_t i = r.rows().begin(); i != r.rows().end(); ++i) {
      for (size_t j = r.cols().begin(); j != r.cols().end(); ++j) {
        my_result[i][j] = my_M1[i][j] + my_M4[i][j] - my_M5[i][j] + my_M7[i][j];
        my_result[i][j + my_colPart] = my_M3[i][j] + my_M5[i][j];
        my_result[i + my_rowPart][j] = my_M2[i][j] + my_M4[i][j];
        my_result[i + my_rowPart][j + my_colPart] =
            my_M1[i][j] - my_M2[i][j] + my_M3[i][j] + my_M6[i][j];
      }
    }
  }

  CombineMatBlocks(vector<vector<float>> &M1, vector<vector<float>> &M2,
                   vector<vector<float>> &M3, vector<vector<float>> &M4,
                   vector<vector<float>> &M5, vector<vector<float>> &M6,
                   vector<vector<float>> &M7, vector<vector<float>> &result,
                   int rowPart, int colPart)
      : my_M1(M1), my_M2(M2), my_M3(M3), my_M4(M4), my_M5(M5), my_M6(M6),
        my_M7(M7), my_result(result), my_rowPart(rowPart), my_colPart(colPart) {
  }
};

// row major to column major
void formatB(vector<vector<float>> &formattedB, vector<vector<float>> &B) {
  int size = B.size();

  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      formattedB[i][j] = B[j][i];
    }
  }
}

//----------------------------------------------------------
// Main algorithm
class strassenMMult : public task {
public:
  // A * B = result
  vector<vector<float>> &my_A;
  vector<vector<float>> &my_B;
  vector<vector<float>> &my_result;

  // constructor
  strassenMMult(vector<vector<float>> &A, vector<vector<float>> &B,
                vector<vector<float>> &result)
      : my_A(A), my_B(B), my_result(result) {}

  task *execute() {
    if (my_A.size() <= threshold) { // optimal value for parallel n^3 algorithm
      size_t size = my_A.size();
      vector<vector<float>> formattedB(size, vector<float>(size));
      formatB(formattedB, my_B);
      parallel_for(blocked_range2d<size_t>(0, size, 16, 0, size, 32),
                   Multiply(my_A, formattedB, my_result), simple_partitioner());
      return NULL;
    }
    // cout << "execute test" << endl;
    int rowPart = my_A.size() / 2;
    int colPart = my_A[1].size() / 2;

    int count = 8; // root + M1 ... M7
    set_ref_count(count);
    // M1 = (A11 + A22)*(B11 + B22)

    // declare temp vectors
    vector<vector<float>> tempAM1;
    vector<vector<float>> tempBM1;
    vector<vector<float>> M1;
    // set sizes
    tempAM1.resize(rowPart, vector<float>(colPart, 0));
    tempBM1.resize(rowPart, vector<float>(colPart, 0));
    M1.resize(rowPart, vector<float>(colPart, 0));
    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempAM1, 0, 1, my_A), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempBM1, 0, 1, my_B), simple_partitioner());
    strassenMMult &task1 =
        *new (allocate_child()) strassenMMult(tempAM1, tempBM1, M1);
    spawn(task1);

    // M2 = (A21 + A22)*B11

    // declare temp vectors
    vector<vector<float>> tempAM2;
    vector<vector<float>> tempBM2;
    vector<vector<float>> M2;
    // set sizes
    tempAM2.resize(rowPart, vector<float>(colPart, 0));
    tempBM2.resize(rowPart, vector<float>(colPart, 0));
    M2.resize(rowPart, vector<float>(colPart, 0));
    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempAM2, 1, 1, my_A), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CopyMatBlocks(tempBM2, 0, my_B), simple_partitioner());
    strassenMMult &task2 =
        *new (allocate_child()) strassenMMult(tempAM2, tempBM2, M2);
    spawn(task2);

    // M3 = A11*(B12 – B22)

    // declare temp vectors
    vector<vector<float>> tempAM3;
    vector<vector<float>> tempBM3;
    vector<vector<float>> M3;
    // set sizes
    tempAM3.resize(rowPart, vector<float>(colPart, 0));
    tempBM3.resize(rowPart, vector<float>(colPart, 0));
    M3.resize(rowPart, vector<float>(colPart, 0));
    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CopyMatBlocks(tempAM3, 0, my_A), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 SubMatBlocks(tempBM3, 1, my_B), simple_partitioner());
    strassenMMult &task3 =
        *new (allocate_child()) strassenMMult(tempAM3, tempBM3, M3);
    spawn(task3);

    // M4 = A22*(B21 – B11)

    // declare temp vectors
    vector<vector<float>> tempAM4;
    vector<vector<float>> tempBM4;
    vector<vector<float>> M4;
    // set sizes
    tempAM4.resize(rowPart, vector<float>(colPart, 0));
    tempBM4.resize(rowPart, vector<float>(colPart, 0));
    M4.resize(rowPart, vector<float>(colPart, 0));
    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CopyMatBlocks(tempAM4, 1, my_A), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 SubMatBlocks(tempBM4, 0, my_B), simple_partitioner());
    strassenMMult &task4 =
        *new (allocate_child()) strassenMMult(tempAM4, tempBM4, M4);
    spawn(task4);

    // M5 = (A11 + A12)*B22

    // declare temp vectors
    vector<vector<float>> tempAM5;
    vector<vector<float>> tempBM5;
    vector<vector<float>> M5;
    // set sizes
    tempAM5.resize(rowPart, vector<float>(colPart, 0));
    tempBM5.resize(rowPart, vector<float>(colPart, 0));
    M5.resize(rowPart, vector<float>(colPart, 0));
    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempAM5, 0, 0, my_A), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CopyMatBlocks(tempBM5, 1, my_B), simple_partitioner());
    strassenMMult &task5 =
        *new (allocate_child()) strassenMMult(tempAM5, tempBM5, M5);
    spawn(task5);

    // M6 = (A21 - A11)*(B11 + B12)

    // declare temp vectors
    vector<vector<float>> tempAM6;
    vector<vector<float>> tempBM6;
    vector<vector<float>> M6;
    // set sizes
    tempAM6.resize(rowPart, vector<float>(colPart, 0));
    tempBM6.resize(rowPart, vector<float>(colPart, 0));
    M6.resize(rowPart, vector<float>(colPart, 0));
    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 SubMatBlocks(tempAM6, 0, my_A), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempBM6, 0, 0, my_B), simple_partitioner());
    strassenMMult &task6 =
        *new (allocate_child()) strassenMMult(tempAM6, tempBM6, M6);
    spawn(task6);

    // M7 = (A12 - A22)*(B21 + B22)

    // declare temp vectors
    vector<vector<float>> tempAM7;
    vector<vector<float>> tempBM7;
    vector<vector<float>> M7;
    // set sizes
    tempAM7.resize(rowPart, vector<float>(colPart, 0));
    tempBM7.resize(rowPart, vector<float>(colPart, 0));
    M7.resize(rowPart, vector<float>(colPart, 0));
    // Calc temp vectors
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 SubMatBlocks(tempAM7, 1, my_A), simple_partitioner());
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 AddMatBlocks(tempBM7, 1, 1, my_B), simple_partitioner());
    strassenMMult &task7 =
        *new (allocate_child()) strassenMMult(tempAM7, tempBM7, M7);
    spawn_and_wait_for_all(task7);
    parallel_for(blocked_range2d<size_t>(0, rowPart, 16, 0, colPart, 32),
                 CombineMatBlocks(M1, M2, M3, M4, M5, M6, M7, my_result,
                                  rowPart, colPart),
                 simple_partitioner());

    return NULL;
  }
};

int main() {
  vector<vector<float>> a(matSize, vector<float>(matSize));
  vector<vector<float>> b(matSize, vector<float>(matSize));
  vector<vector<float>> c(matSize, vector<float>(matSize));

  // Setup a
  for (int i = 0; i < (matSize); i++) {
    for (int j = 0; j < (matSize); j++) {
      a[i][j] = i * j + 1;
      // a[i][j] = 1;
    }
  }

  // Setup b
  for (int i = 0; i < (matSize); i++) {
    for (int j = 0; j < (matSize); j++) {
      b[i][j] = matSize * matSize - i * j - 1;
      // b[i][j] = 1;
    }
  }

  cout << "Starting calculation." << endl;

  tick_count t0 = tick_count::now();
  strassenMMult &test = *new (task::allocate_root()) strassenMMult(a, b, c);
  task::spawn_root_and_wait(test);
  tick_count t1 = tick_count::now();

  cout << "Finished" << endl;
  cout << "Calculation took: " << (t1 - t0).seconds() << " seconds." << endl;

  cout << "Test value: " << c[1234][456] << endl;

  // for( int i=0; i<matSize; ++i ){
  //   for( int j=0; j<matSize; ++j ){
  //     cout << c[i][j] << " ";
  //   }
  //   cout << endl;
  // }
}
