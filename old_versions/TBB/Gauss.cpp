#include "tbb/blocked_range.h"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <math.h>

using namespace tbb;
using namespace std;

//         Setup
//-------------------------
// Matrix size
int M = 10000;
int N = 10000;

// Gauss parameters
const int KernelSize = 3;
const double sigma = 0.391;
//--------------------------

// derived parameters
int radius = KernelSize / 2;

float *a;
float *blurred_a;
float kernel[KernelSize];

class gaussianBlurXBody {

public:
  void operator()(const blocked_range2d<int> &r) const {
    for (int i = r.rows().begin(); i != r.rows().end(); ++i) {
      for (int j = r.cols().begin(); j != r.cols().end(); ++j) {
        float sum = 0;
        for (int k = 0; k < KernelSize; ++k) {
          // if (!((j - radius + k) < 0 || (j - radius + k) > (N - 1)))
          sum += a[i * N + j - radius + k] * kernel[k];
        }
        blurred_a[i * N + j] = sum;
      }
    }
  }
};

class gaussianBlurXEdgeBody {

public:
  void operator()(const blocked_range<int> &r) const {
    // rows with missing entries
    for (int i = r.begin(); i != r.end(); ++i) {
      // columns with missing entries
      for (int j1 = 0; j1 < radius; j1++) {
        float sum1 = 0;
        float sum2 = 0;
        int j2 = (N - 1 - j1);
        for (int k = 0; k < KernelSize; ++k) {
          if (!((j1 - radius + k) < 0))
            sum1 += a[i * N + (j1 - radius + k)] * kernel[k];
          if (!((j2 - radius + k) > (N - 1)))
            sum2 += a[i * N + (j2 - radius + k)] * kernel[k];
        }
        blurred_a[i * N + j1] = sum1;
        blurred_a[i * N + j2] = sum2;
      }
    }
  }
};

class gaussianBlurYBody {

public:
  void operator()(const blocked_range2d<int> &r) const {
    for (int i = r.rows().begin(); i != r.rows().end(); ++i) {
      for (int j = r.cols().begin(); j != r.cols().end(); ++j) {
        float sum = 0;
        for (int k = 0; k < KernelSize; ++k) {
          // if (!((i - radius + k) < 0 || (i - radius + k) > (N - 1)))
          sum += blurred_a[(i - radius + k) * N + j] * kernel[k];
        }
        a[i * N + j] = sum;
      }
    }
  }
};

class gaussianBlurYEdgeBody {

public:
  void operator()(const blocked_range<int> &r) const {
    // rows with missing entries
    for (int j = r.begin(); j != r.end(); ++j) {
      // columns with missing entries
      for (int i1 = 0; i1 < radius; i1++) {
        float sum1 = 0;
        float sum2 = 0;
        int i2 = (M - 1 - i1);
        for (int k = 0; k < KernelSize; ++k) {
          if (!((i1 - radius + k) < 0))
            sum1 += blurred_a[(i1 - radius + k) * N + j] * kernel[k];
          if (!((i2 - radius + k) > (N - 1)))
            sum2 += blurred_a[(i2 - radius + k) * N + j] * kernel[k];
        }
        a[i1 * N + j] = sum1;
        a[i2 * N + j] = sum2;
      }
    }
  }
};

void calcKernel() {
  // Calculate Gauss Distribution
  float factor = 1 / (sqrt(2 * M_PI) * sigma);
  float total = 0;

  // Set 1D Kernel
  for (int i = -radius; i <= radius; i++) {
    float value = exp(-(i * i) / (2 * sigma * sigma));
    value = factor * value;
    total += value;
    kernel[i + radius] = value;
  }
  // Set 1D nomalized Kernel
  for (int i = 0; i < KernelSize; i++) {
    kernel[i] = kernel[i] / total;
  }
}

void printResult() {
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      cout << a[i * N + j] << " ";
    }
    cout << endl;
  }
}

void run() {
  // Input a and b Output c

  a = new float[M * N];
  blurred_a = new float[M * N];

  // Setup a
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      a[i * N + j] = i * j;
      // a[i][j] = 0;
    }
  }

  // a[1][1] = 1;

  // calculate Kernel
  calcKernel();

  // Start calculation 1
  // cout << "Starting calculation" << endl;
  tick_count t0 = tick_count::now();

  // X Convolution
  parallel_for(blocked_range2d<int>(0, M, radius, N - radius),
               gaussianBlurXBody());

  // X Convolution edges
  parallel_for(blocked_range<int>(0, M, 16), gaussianBlurXEdgeBody());

  // Y Convolution
  parallel_for(blocked_range2d<int>(radius, M - radius, 0, N),
               gaussianBlurYBody());

  // Y Convolution edges
  parallel_for(blocked_range<int>(0, N, 16), gaussianBlurYEdgeBody());

  tick_count t2 = tick_count::now();

  printf("size: %i\ttime: %f s\n", M, (t2 - t0).seconds());
  // cout << "Test value: " << a[345 * N + 24] << " ." << endl;
  // cout << "Test value: " << a[345 * N + 0] << " ." << endl;
  // cout << "Test value: " << a[0 * N + 24] << " ." << endl;
  // cout << "Test value: " << a[1234 * N + 1234] << " ." << endl;
  // cout << "Test value: " << a[1999 * N + 1999] << " ." << endl;

  // printResult();
  delete[](a);
  delete[](blurred_a);
}

int main() {
  printf("TBB:\n");
  // for (int i = 5000; i <= 50000; i += 5000) {
  //   M = i;
  //   N = i;
  //   run();
  // }
  run();
}
