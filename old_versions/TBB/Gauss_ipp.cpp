#include <stdio.h>
#include "ipp.h"
#include <iostream>
#include <math.h>
#include "tbb/tick_count.h"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"


using namespace std;
using namespace tbb;

//Global parameters setup filter and input
int width = 15000;
int height = 15000;
int roiX = 1000;  /*Size of a single ROI X*/
int roiY = 1000;  /*                    Y*/
Ipp32f sigma = 0.391f;
Ipp32u kernelSize = 3;

/* Next two defines are created to simplify code reading and understanding */
#define EXIT_MAIN exitLine:                                  /* Label for Exit */



//Global parameters dont change anything
IppStatus status = ippStsNoErr;
Ipp32f* pSrc = NULL, *pDst = NULL;     /* Pointers to source/destination images */
int srcStep = 1, dstStep = 0;          /* Steps, in bytes, through the source/destination images */
IppiSize imageSize = {width, height};  /* Size of source/destination ROI in pixels */
IppiSize roiSize = {roiX,roiY};        /* 4x4 Region of interest*/
int radius = ceil(kernelSize/2.0);     /*radius of filter needed for padding */
int rad2 = kernelSize/2;
Ipp8u borderValue = 0;                 /*Value for edge cases*/
int numChannels = 1;                   /*no color channels just array of single entries*/

//Process a single ROI of an image
void processROI(IppiSize roiSize,
              Ipp32f* pSrcRoi, Ipp32f* pDstRoi, int srcStep, int dstStep,
              IppiBorderType borderType=ippBorderInMem){

  //initialize required parameters
  Ipp8u *pBuffer = NULL;                 /* Pointer to the work buffer */
  IppFilterGaussianSpec* pSpec = NULL;   /* context structure */
  int iTmpBufSize = 0, iSpecSize = 0;    /* Common work buffer size */


  ippiFilterGaussianGetBufferSize(roiSize, kernelSize, ipp32f,
      numChannels, &iSpecSize, &iTmpBufSize);


  pSpec = (IppFilterGaussianSpec *)ippsMalloc_8u(iSpecSize);
  pBuffer = ippsMalloc_8u(iTmpBufSize);

  //init Gauss kernel with given parameters
  ippiFilterGaussianInit(roiSize, kernelSize, sigma,
      borderType, ipp32f, numChannels, pSpec, pBuffer);

  //apply Gauss filter
  ippiFilterGaussianBorder_32f_C1R(pSrcRoi, srcStep, pDstRoi, dstStep,
      roiSize, borderValue, pSpec, pBuffer);

  //free memory of temporary buffers
  ippsFree(pBuffer);
  ippsFree(pSpec);
}

void run()
{
    //Start timer
    tick_count t0 = tick_count::now();

    //run parallel_for loop
    parallel_for(blocked_range2d<int>(0, ceil(height/(float)roiY), 0,ceil(width/(float)roiX)),
      [=](const blocked_range2d<int> &r) {
        for (int i = r.rows().begin(); i != r.rows().end(); ++i) {
          for (int j = r.cols().begin(); j != r.cols().end(); ++j) {

            Ipp32f* pSrcRoi =  &pSrc[((i*roiY)+rad2)*srcStep/4+(j*roiX)+rad2];
            Ipp32f* pDstRoi =  &pDst[((i*roiY)+rad2)*dstStep/4+(j*roiX)+rad2];

            int altY = ((i+1)*roiY>height) ? (height-i*roiY) : (roiY);
            int altX = ((j+1)*roiX>width) ? (width-j*roiX) : (roiX);

            //process a single ROI of size altX * altY
            processROI({altX,altY}, pSrcRoi, pDstRoi, srcStep, dstStep);
        }
      }
    });


    //end timer
    tick_count t1 = tick_count::now();

    printf("size: %i, roiSize: %i * %i, time: %f s\n", width,roiX,roiY,(t1 - t0).seconds());

   //print values like this
    auto printValue = [](int x, int y) {
      cout << "Test value: " << pDst[(x + rad2) * dstStep / 4 + (y + rad2)] << endl;
    };
    //print values
    printValue(345,24);printValue(345,0); printValue(0,24);
    printValue(234,234);printValue(999,999); printValue(9999,9999);
}

int main(){
  printf("IPP:\n");

  //allocate memory for input and output (padded for borders)
  pSrc = ippiMalloc_32f_C1(imageSize.width+2*rad2, imageSize.height+2*rad2, &srcStep);
  pDst = ippiMalloc_32f_C1(imageSize.width+2*rad2, imageSize.height+2*rad2, &dstStep);

  //setup values for whole image
  for(int i = rad2; i < height+rad2; i++){
    for(int j = rad2; j < width+rad2; j++){
     pSrc[i*srcStep/4+j] = (i-rad2)*(j-rad2); /* access of cache aligned entry [i][j]*/
    }
  }

  //calculate Gauss Blur
  run();

  //free memory for input and output
  ippiFree(pSrc);
  ippiFree(pDst);

  return 0;
}
