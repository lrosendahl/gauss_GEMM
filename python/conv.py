from scipy.signal import convolve
import numpy as np
import sys

size = 16
image = np.zeros((size, size))

for i in range(0,size):
    for j in range(0,size):
        image[i][j] = i*j+1

# Gauss kernel with parameters size=3 sigma=0.391 normalized
kernel = [[0.00124641, 0.0328117, 0.00124641],
       [0.0328117, 0.863767, 0.0328117],
       [0.00124641, 0.0328117, 0.00124641]]

edge = 1
out = convolve(image, kernel)

for i in range(0, size):
    for j in range(0, size):
        if i < edge or i >= (size-edge) or j < edge or j >= (size-edge):
            sys.stdout.write("{}, ".format(0))
        else:
            sys.stdout.write("{}, ".format(round(out[edge+i][edge+j], 5)))
    print("")
print("")
