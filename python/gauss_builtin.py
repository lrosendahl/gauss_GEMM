from scipy.ndimage.filters import gaussian_filter
import numpy as np
import time

#Setup test_arr
size = 16
test_arr = np.zeros((size, size))
#test_arr[4][4] = 1
for i in range(0,size):
    for j in range(0,size):
        test_arr[i][j] = i*j

#Start calculation
print("Starting calculation")
start = time.time()


#caluclate truncate value for set window size
w = 3 #window size
s = 0.391 #sigma
t = (((w - 1)/2)-0.5)/s #truncate value

#Filter array with gaussian kernel
blurred = gaussian_filter(test_arr, sigma=s,truncate=t,mode='constant')

finish = time.time()

#Show results
# print("Calculation took " + str(finish - start) + " seconds.")
# print("Test value: " + str(round(blurred[345][24],5)))
# print("Test value: " + str(round(blurred[345][0],5)))
# print("Test value: " + str(round(blurred[0][24],5)))
# print("Test value: " + str(round(blurred[1234][1234],5)))
# print("Test value: " + str(round(blurred[1999][1999],5)))


# for i in range(0,size):
#     for j in range(0,size):
#         print(round(blurred[i][j],5)),
#     print("")
