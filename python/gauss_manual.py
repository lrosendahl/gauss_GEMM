from scipy.signal import convolve
import numpy as np
import time

#input matrix
size = 2000
in1 = np.zeros((size, size))

for i in range(0,size):
    for j in range(0,size):
        in1[i][j] = i*j

#example kernel size=3 sigma=0.391 normalized
in2 = [[0.00124641, 0.0328117, 0.00124641],
       [0.0328117, 0.863767, 0.0328117],
       [0.00124641, 0.0328117, 0.00124641]]

radius = 1

#start calculation
start = time.time()

out = convolve(in1,in2,method='direct')

finish = time.time()

#Show results
print("Calculation took " + str(finish - start) + " seconds.")
print("Test value: " + str(round(out[345+radius][24+radius],5)))
print("Test value: " + str(round(out[345+radius][0+radius],5)))
print("Test value: " + str(round(out[0+radius][24+radius],5)))
print("Test value: " + str(round(out[1234+radius][1234+radius],5)))
print("Test value: " + str(round(out[1999+radius][1999+radius],5)))

#print output and remove edges
# for i in range(1,len(out)-1):
#     for j in range(1,len(out[0])-1):
#         print(round(out[i][j],5)),
#     print("")
