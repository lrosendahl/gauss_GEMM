#ifndef BENCHMARK
#define BENCHMARK

class Benchmarkable {
public:
  virtual void run() = 0;

  ~Benchmarkable() {}
};
#endif
