#include "Benchmarkable.cpp"
#include "gauss/GaussIPP.cpp"
#include "gauss/GaussTBB.cpp"
#include "gauss/GaussTBBIPP.cpp"
#include "gemm/GemmMKL.cpp"
#include <limits>
// #include "gemm/GemmOpenBLAS.cpp"
#include "gemm/GemmTBBv1.cpp"
#include "gemm/GemmTBBv2.cpp"
#include "gemm/GemmTBBv3.cpp"
#include "gemm/GemmTBBv3Const.cpp"
#include "tbb/tick_count.h"
#include <iostream>
#include <string.h>

float initValues(int i, int j) { return i * j + 1; }

float time(Benchmarkable &benchmark) {
  // Three warmups
  benchmark.run();
  benchmark.run();
  benchmark.run();

  // 30 runs, return minimum
  float best = std::numeric_limits<float>::max();
  int runs = 300;

  for (int i = 0; i < runs; i++) {
    tbb::tick_count start = tbb::tick_count::now();
    benchmark.run();
    auto t = (tbb::tick_count::now() - start).seconds();

    if (t < best) {
      best = t;
    }
  }

  return best;
}

void showUsage() {
  printf("Usage: benchmarks <operation> <library> <size>\n");
  printf("<operation>: gauss|gemm\n");
  printf("<library>: mkl|tbb-v1|tbb-v2|tbb-v3|tbb-v3-const (ignores "
         "size) for "
         "gemm, "
         "ipp|tbb|tbb-ipp for "
         "gauss\n");
  printf("<size>: Integer\n");
  printf("Example: benchmarks gauss 1024 - Runs gauss filter at size 1024\n");
}

int main(int argc, char *argv[]) {
  if (argc < 4) {
    showUsage();
  } else {
    int size = std::stoi(argv[3]);
    if (!strcmp(argv[1], "gemm")) {
      if (!strcmp(argv[2], "mkl")) {
        GemmMKL gemmMKL(size, size, size, initValues, initValues);
        printf("GEMM MKL: %fs\n", time(gemmMKL));
        // } else if (!strcmp(argv[2], "openblas")) {
        //   GemmOpenBLAS gemmOpenBLAS(size, size, size, initValues,
        //   initValues);
        //   printf("GEMM OpenBlas: %fs\n", time(gemmOpenBLAS));
      } else if (!strcmp(argv[2], "tbb-v1")) {
        GemmTBBv1 gemmTBBv1(size, size, size, initValues, initValues);
        printf("GEMM TBB (v1): %fs\n", time(gemmTBBv1));
      } else if (!strcmp(argv[2], "tbb-v2")) {
        GemmTBBv2 gemmTBBv2(size, size, size, initValues, initValues);
        printf("GEMM TBB (v2): %fs\n", time(gemmTBBv2));
      } else if (!strcmp(argv[2], "tbb-v3")) {
        gemmTBBv3::GemmTBBv3 gemmTBBv3(size, size, size, initValues,
                                       initValues);
        printf("GEMM TBB (v3): %fs\n", time(gemmTBBv3));
      } else if (!strcmp(argv[2], "tbb-v3-const")) {
        gemmTBBv3Const::GemmTBBv3Const gemmTBBv3Const(initValues, initValues);
        printf("GEMM TBB (v3 const, SIZE PARAMETER IGNORED): %fs\n",
               time(gemmTBBv3Const));
      } else {
        showUsage();
      }
    } else if (!strcmp(argv[1], "gauss")) {
      if (!strcmp(argv[2], "ipp")) {
        GaussIPP gaussIPP(size, size, initValues);
        printf("Gauss IPP: %fs\n", time(gaussIPP));
      } else if (!strcmp(argv[2], "tbb")) {
        GaussTBB gaussTBB(size, size, initValues);
        printf("Gauss TBB: %fs\n", time(gaussTBB));
      } else if (!strcmp(argv[2], "tbb-ipp")) {
        GaussTBBIPP gaussTBBIPP(size, size, initValues);
        printf("Gauss TBB+IPP: %fs\n", time(gaussTBBIPP));
      } else {
        showUsage();
      }
    } else {
      showUsage();
    }
  }
}
