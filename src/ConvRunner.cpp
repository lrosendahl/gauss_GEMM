#include "Benchmarkable.cpp"
#include "conv/Conv2DParallel.cpp"
#include "conv/Conv2DSerial.cpp"
#include "conv/Conv3DParallel.cpp"
#include "conv/Conv3DSerial.cpp"
#include "tbb/tick_count.h"
#include <stdio.h>

// Gauss kernel with parameters size=3 sigma=0.391 normalized
float gaussKernel[] = {0.00124641, 0.0328117,  0.00124641, 0.0328117, 0.863767,
                       0.0328117,  0.00124641, 0.0328117,  0.00124641};

// Used to initialize image
float initImage2D(int i, int j) { return i * j + 1; }
float initImage3D(int i, int j, int k) { return i * j * k + 1; }

// 3D Jacobi Kernel
float jacobiKernel[] = {0, 0,     0, 0,     0.165, 0,     0, 0,     0,
                        0, 0.164, 0, 0.162, -1.67, 0.161, 0, 0.163, 0,
                        0, 0,     0, 0,     0.166, 0,     0, 0,     0};

float time(Benchmarkable &benchmark) {
  tbb::tick_count start = tbb::tick_count::now();
  benchmark.run();
  printf("%f\n", (tbb::tick_count::now() - start).seconds() * 1000);
}

void runGauss2D(int size, bool parallel) {
  int inputShape[] = {size, size};
  int kernelShape[] = {3, 3};

  float *input = new float[inputShape[0] * inputShape[1]];
  float *output = new float[inputShape[0] * inputShape[1]];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      input[i * inputShape[0] + j] = initImage2D(i, j);
      output[i * inputShape[0] + j] = 0;
    }
  }

  if (parallel) {
    Conv2DParallel conv(input, output, gaussKernel, inputShape, kernelShape);
    time(conv);
  } else {
    Conv2DSerial conv(input, output, gaussKernel, inputShape, kernelShape);
    time(conv);
  }

  delete input;
  delete output;
}

void runJacobi3D(int size, bool parallel) {
  int inputShape[] = {size, size, size};
  int kernelShape[] = {3, 3, 3};

  float *input = new float[inputShape[0] * inputShape[1] * inputShape[2]];
  float *output = new float[inputShape[0] * inputShape[1] * inputShape[2]];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0] * inputShape[1] * inputShape[2]; i++) {
    input[i] = (i % 10) + 1;
    output[i] = 0;
  }

  if (parallel) {
    Conv3DParallel conv(input, output, jacobiKernel, inputShape, kernelShape);
    time(conv);
  } else {
    Conv3DSerial conv(input, output, jacobiKernel, inputShape, kernelShape);
    time(conv);
  }

  delete input;
  delete output;
}

void showUsage() {
  printf("Usage: conv-runner gauss2D|jacobi3D serial|parallel <size>\n");
}

int main(int argc, char *argv[]) {
  if (argc < 4) {
    showUsage();
    return 0;
  }

  int size = std::stoi(argv[3]);

  if (!strcmp(argv[1], "gauss2D") && !strcmp(argv[2], "serial")) {
    runGauss2D(size, false);
  } else if (!strcmp(argv[1], "gauss2D") && !strcmp(argv[2], "parallel")) {
    runGauss2D(size, true);
  } else if (!strcmp(argv[1], "jacobi3D") && !strcmp(argv[2], "serial")) {
    runJacobi3D(size, false);
  } else if (!strcmp(argv[1], "jacobi3D") && !strcmp(argv[2], "parallel")) {
    runJacobi3D(size, true);
  } else {
    showUsage();
  }

  return 0;
}
