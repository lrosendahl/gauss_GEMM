#include "Timing.cpp"
#include "conv/Conv2DParallel.cpp"
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define SIGMA 15
#define KERNEL_SIZE 91

float initValues(int i, int j) { return rand() % 256; }

int main(int argc, char *argv[]) {
  int inputShape[] = {6000, 4000};
  int kernelShape[] = {KERNEL_SIZE, KERNEL_SIZE};

  float *input = new float[inputShape[0] * inputShape[1]];
  float *output = new float[inputShape[0] * inputShape[1]];
  float *kernel = new float[KERNEL_SIZE * KERNEL_SIZE];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      input[i * inputShape[1] + j] = initValues(i, j);
      output[i * inputShape[1] + j] = 0;
    }
  }

  // Calculate Gauss Distribution
  float factor = 1 / (sqrt(2 * M_PI) * SIGMA);
  float total = 0;
  int radius = KERNEL_SIZE / 2;

  // Set 1D Kernel
  for (int i = -radius; i <= radius; i++) {
    float value = exp(-(i * i) / (2 * SIGMA * SIGMA));
    value = factor * value;
    total += value;
    kernel[i + radius] = value;
  }

  // Set 1D nomalized Kernel
  for (int i = 0; i < KERNEL_SIZE; i++) {
    kernel[i] = kernel[i] / total;
  }

  Conv2DParallel conv(input, output, kernel, inputShape, kernelShape);

  printf("Gauss TBB: %fs\n", time(conv));

  // TODO: This seems to cause a SegFault
  // delete input;
  // delete output;
  // delete kernel;
}
