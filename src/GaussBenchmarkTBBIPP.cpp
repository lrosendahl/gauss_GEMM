#include "gauss/GaussTBBIPP.cpp"
#include <iostream>
#include <limits>
#include <stdlib.h>
#include <string.h>

float time(Benchmarkable &benchmark) {
  // Three warmups
  benchmark.run();
  benchmark.run();
  benchmark.run();

  float best = std::numeric_limits<float>::max();
  int runs = 100;

  for (int i = 0; i < runs; i++) {
    tbb::tick_count start = tbb::tick_count::now();
    benchmark.run();
    auto t = (tbb::tick_count::now() - start).seconds();

    if (t < best) {
      best = t;
    }
  }

  return best;
}

float initValues(int i, int j) { return rand() % 256; }

int main(int argc, char *argv[]) {
  GaussTBBIPP gaussTBBIPP(6000, 4000, initValues);
  printf("Gauss TBB+IPP: %fs\n", time(gaussTBBIPP));
}
