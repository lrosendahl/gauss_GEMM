#include "InitValues.cpp"
#include "Timing.cpp"
#include "gemm/GemmMKL.cpp"
#include <iostream>

#define SIZE 1024

int main() {
  GemmMKL gemmMKL(SIZE, SIZE, SIZE, initValues, initValues);
  auto t = time(gemmMKL);
  printf("%f\n", t * 1000);
}
