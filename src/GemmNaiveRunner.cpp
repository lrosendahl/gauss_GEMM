#include "InitValues.cpp"
#include "Timing.cpp"
#include "gemm/GemmNaive.cpp"
#include <iostream>

int main() {
  gemmNaive::GemmNaive gemm(1024, 1024, 1024, initValues, initValues);
  auto t = time(gemm);
  printf("%f\n", t * 1000);
}
