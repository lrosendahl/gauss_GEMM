#include "Benchmarkable.cpp"
#include "InitValues.cpp"
#include "Timing.cpp"
#include "gemm/GemmTBBv3.cpp"
#include "tbb/tick_count.h"
#include <iostream>

#define SIZE 4096

int main() {
  gemmTBBv3::GemmTBBv3 gemmTBB(SIZE, SIZE, SIZE, initValues, initValues);
  auto t = time(gemmTBB);
  printf("%f\n", t * 1000);
}
