#include "Benchmarkable.cpp"
#include "gemm/GemmTBBv4.cpp"
#include "tbb/tick_count.h"
#include <iostream>

float initValues(int i, int j) { return i * j + 1; }

float time(Benchmarkable &benchmark) {
  tbb::tick_count start = tbb::tick_count::now();
  benchmark.run();
  return (tbb::tick_count::now() - start).seconds();
}

int main() {
  gemmTBBv4::GemmTBBv4 gemmTBB(initValues, initValues);
  auto t = time(gemmTBB);
  printf("%f\n", t * 1000);
}
