#include "InitValues.cpp"
#include "Timing.cpp"
#include "gemm/GemmTBBv5a.cpp"
#include <iostream>

int main() {
  gemmTBBv5a::GemmTBBv5a gemmTBB(initValues, initValues);
  auto t = time(gemmTBB);
  printf("%f\n", t * 1000);
}
