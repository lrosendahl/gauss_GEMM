#include "SparseMatrix.cpp"

float initValues(int i, int j) { return i * j + 1; }
const int numValues = 13;
float values[numValues] = {1, -1, -3, -2, 5, 4, 6, 4, -4, 2, 7, 8, -5};
int columns[numValues] = {0, 1, 3, 0, 1, 2, 3, 4, 0, 2, 3, 1, 4};
int pointerB[5] = {0, 3, 5, 8, 11};
int pointerE[5] = {3, 5, 8, 11, 13};

SparseMatrix getSparseMatrix(int size, int p) {
  float *a = new float[size * size];

  int valueA = 0;

  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      valueA = 0;

      if (((i * j) % p) == 0) {
        valueA = i * j + 1;
      }

      a[i * size + j] = valueA;
    }
  }

  SparseMatrix spm = SparseMatrix(a, size, size);

  return spm;
}

SparseMatrix getDiagonalSparseMatrix(int size) {
  float *a = new float[size * size];

  int valueA = 0;

  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      valueA = 0;

      if (i == j) {
        valueA = i * j + 1;
      }

      a[i * size + j] = valueA;
    }
  }

  SparseMatrix spm = SparseMatrix(a, size, size);

  return spm;
}
