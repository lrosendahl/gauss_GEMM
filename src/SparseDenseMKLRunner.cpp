#include "Benchmarkable.cpp"
#include "InitValues.cpp"
#include "Timing.cpp"
#include "spmm/SparseDenseMKL.cpp"
#include "tbb/tick_count.h"
#include <iostream>

#define SIZE 1024

int main() {
  SparseMatrix spmA = getDiagonalSparseMatrix(SIZE);

  SparseDenseMKL sparseDenseMKL(
      SIZE, SIZE, SIZE, spmA.getNumValues(), spmA.getValues(),
      spmA.getColumns(), spmA.getPointerB(), spmA.getPointerE(), initValues);

  auto t = time(sparseDenseMKL);
  printf("%f\n", t * 1000);
}
