#include "Benchmarkable.cpp"
#include "InitValues.cpp"
#include "Timing.cpp"
#include "spmm/SparseDenseTBB.cpp"
#include "tbb/tick_count.h"
#include <iostream>

#define SIZE 1024

int main() {
  SparseMatrix spmA = getDiagonalSparseMatrix(SIZE);

  sparseDenseTBB::SparseDenseTBB sparseDenseTBB(
      SIZE, SIZE, SIZE, spmA.getNumValues(), spmA.getValues(),
      spmA.getColumns(), spmA.getPointerB(), spmA.getPointerE(), initValues);

  auto t = time(sparseDenseTBB);
  printf("%f\n", t * 1000);
}
