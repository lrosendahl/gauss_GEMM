class SparseMatrix {
private:
  int numValues = 0;
  float *values;
  int *columns;
  int *pointerB;
  int *pointerE;

public:
  SparseMatrix(float *a, int m, int n) {
    // Count non-zero values
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (a[i * n + j] != 0) {
          numValues++;
        }
      }
    }

    // Allocate arrays
    values = new float[numValues];
    columns = new int[numValues];
    pointerB = new int[m];
    pointerE = new int[m];

    // Fill arrays
    int pos = 0;
    int begin = 0;
    int end = 0;

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        int value = a[i * n + j];

        if (value != 0) {
          values[pos] = value;
          columns[pos] = j;
          pos++;
          end++;
        }
      }

      pointerB[i] = begin;
      pointerE[i] = end;
      begin = end;
    }
  }

  int getNumValues() { return numValues; }

  float *getValues() { return values; }

  int *getColumns() { return columns; }

  int *getPointerB() { return pointerB; }

  int *getPointerE() { return pointerE; }

  ~SparseMatrix() {
    delete[](values);
    delete[](columns);
    delete[](pointerB);
    delete[](pointerE);
  }
};
