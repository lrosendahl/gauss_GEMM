#include "SparseMatrix.cpp"
#include <boost/test/unit_test.hpp>

// Matrix in normal format
int m = 5;
int n = 5;
float a[] = {1, -1, 0,  -3, 0, -2, 5, 0, 0, 0, 0, 0, 4,
             6, 4,  -4, 0,  2, 7,  0, 0, 8, 0, 0, -5};

// Matrix in CSR format
const int numValues = 13;
float values[numValues] = {1, -1, -3, -2, 5, 4, 6, 4, -4, 2, 7, 8, -5};
int columns[numValues] = {0, 1, 3, 0, 1, 2, 3, 4, 0, 2, 3, 1, 4};
int pointerB[5] = {0, 3, 5, 8, 11};
int pointerE[5] = {3, 5, 8, 11, 13};

int main() {
  printf("Running sparse matrix test...\n");
  SparseMatrix spm = SparseMatrix(a, m, n);

  if (spm.getNumValues() != numValues) {
    printf("numValues incorrect\n");
    return 1;
  }

  for (int i = 0; i < numValues; i++) {
    if (spm.getValues()[i] != values[i]) {
      printf("values incorrect\n");
      return 1;
    }

    if (spm.getColumns()[i] != columns[i]) {
      printf("columns incorrect\n");
      return 1;
    }
  }

  for (int i = 0; i < m; i++) {
    if (spm.getPointerB()[i] != pointerB[i]) {
      printf("pointerB incorrect\n");
      return 1;
    }

    if (spm.getPointerE()[i] != pointerE[i]) {
      printf("pointerE incorrect\n");
      return 1;
    }
  }

  printf("Success\n");
}
