#include "Benchmarkable.cpp"
#include "InitValues.cpp"
#include "Timing.cpp"
#include "spmm/SparseSparseMKL.cpp"
#include "tbb/tick_count.h"
#include <iostream>

#define SIZE 1024

int main() {
  SparseMatrix spmA = getDiagonalSparseMatrix(SIZE);
  SparseMatrix spmB = getDiagonalSparseMatrix(SIZE);

  SparseSparseMKL sparseSparseMKL(
      SIZE, SIZE, SIZE, spmA.getNumValues(), spmA.getValues(),
      spmA.getColumns(), spmA.getPointerB(), spmA.getPointerE(),
      spmB.getNumValues(), spmB.getValues(), spmB.getColumns(),
      spmB.getPointerB(), spmB.getPointerE());

  auto t = time(sparseSparseMKL);
  printf("%f\n", t * 1000);
}
