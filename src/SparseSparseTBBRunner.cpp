#include "Benchmarkable.cpp"
#include "InitValues.cpp"
#include "Timing.cpp"
#include "spmm/SparseSparseTBB.cpp"
#include "tbb/tick_count.h"
#include <iostream>

#define SIZE 1024

int main() {
  SparseMatrix spmA = getDiagonalSparseMatrix(SIZE);
  SparseMatrix spmB = getDiagonalSparseMatrix(SIZE);

  sparseSparseTBB::SparseSparseTBB sparseSparseTBB(
      SIZE, SIZE, SIZE, spmA.getNumValues(), spmA.getValues(),
      spmA.getColumns(), spmA.getPointerB(), spmA.getPointerE(),
      spmB.getNumValues(), spmB.getValues(), spmB.getColumns(),
      spmB.getPointerB(), spmB.getPointerE());

  auto t = time(sparseSparseTBB);
  printf("%f\n", t * 1000);
}
