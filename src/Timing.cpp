#include "Benchmarkable.cpp"
#include "tbb/tick_count.h"
#include <limits>

#ifndef NUM_WARMUPS
#define NUM_WARMUPS 10
#endif

#ifndef NUM_EVALUATIONS
#define NUM_EVALUATIONS 200
#endif

float time(Benchmarkable &benchmark) {

  for (int i = 0; i < NUM_WARMUPS; i++) {
    benchmark.run();
  }

  float min = std::numeric_limits<float>::max();

  for (int i = 0; i < NUM_EVALUATIONS; i++) {
    tbb::tick_count start = tbb::tick_count::now();
    benchmark.run();
    auto t = (tbb::tick_count::now() - start).seconds();

    if (t < min) {
      min = t;
    }
  }

  return min;
}
