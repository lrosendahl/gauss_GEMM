#include "../Benchmarkable.cpp"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"

class Conv2DParallel : public Benchmarkable {
private:
  float *input;
  float *output;
  float *kernel;

  int *inputShape;
  int *kernelShape;
  int kernelRadius[2];

public:
  Conv2DParallel(float *input, float *output, float *kernel, int *inputShape,
                 int *kernelShape)
      : input(input), output(output), kernel(kernel), inputShape(inputShape),
        kernelShape(kernelShape) {
    kernelRadius[0] = kernelShape[0] / 2;
    kernelRadius[1] = kernelShape[1] / 2;
  }

  ~Conv2DParallel() {}

  void run() {
    auto convolve = [=](const tbb::blocked_range2d<int> &r) {
      for (int ii = r.rows().begin(); ii < r.rows().end(); ii++) {
        for (int jj = r.cols().begin(); jj < r.cols().end(); jj++) {
          // [ii, jj] is the position in the output image
          float tmp = 0;
          for (int kk = 0; kk < kernelShape[0]; kk++) {
            for (int ll = 0; ll < kernelShape[1]; ll++) {
              // [kk, ll] is  the position in the kernel
              tmp += input[(ii - kernelRadius[0] + kk) * inputShape[1] +
                           (jj - kernelRadius[1] + ll)] *
                     kernel[kk * kernelShape[1] + ll];
            }
          }
          output[ii * inputShape[1] + jj] = tmp;
        }
      }
    };

    // Check if environment variables are set
    const char *gsx = std::getenv("GRAIN_SIZE_X");
    const char *gsy = std::getenv("GRAIN_SIZE_Y");

    // Use auto partioner by default
    auto range = tbb::blocked_range2d<int>(
        kernelRadius[0], inputShape[0] - kernelRadius[0], kernelRadius[1],
        inputShape[1] - kernelRadius[1]);

    // Unless grain size was set
    if (gsx && gsy) {
      range = tbb::blocked_range2d<int>(
          kernelRadius[0], inputShape[0] - kernelRadius[0], std::stoi(gsx),
          kernelRadius[1], inputShape[1] - kernelRadius[1], std::stoi(gsy));
    }

    tbb::parallel_for(range, convolve);
  }

  float getResult(int i, int j) { return output[i * inputShape[1] + j]; }
};
