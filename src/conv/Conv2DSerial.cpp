#include "../Benchmarkable.cpp"

class Conv2DSerial : public Benchmarkable {
private:
  float *input;
  float *output;
  float *kernel;

  int *inputShape;
  int *kernelShape;
  int kernelRadius[2];

public:
  Conv2DSerial(float *input, float *output, float *kernel, int *inputShape,
             int *kernelShape)
      : input(input), output(output), kernel(kernel), inputShape(inputShape),
        kernelShape(kernelShape) {
    kernelRadius[0] = kernelShape[0] / 2;
    kernelRadius[1] = kernelShape[1] / 2;
  }

  ~Conv2DSerial() {}

  void run() {
    for (int ii = kernelRadius[0]; ii < inputShape[0] - kernelRadius[0]; ii++) {
      for (int jj = kernelRadius[1]; jj < inputShape[1] - kernelRadius[1];
           jj++) {
        // [ii, jj] is the position in the output image
        float tmp = 0;
        for (int kk = 0; kk < kernelShape[0]; kk++) {
          for (int ll = 0; ll < kernelShape[1]; ll++) {
            // [kk, ll] is  the position in the kernel
            tmp += input[(ii - kernelRadius[0] + kk) * inputShape[0] +
                         (jj - kernelRadius[1] + ll)] *
                   kernel[kk * kernelShape[0] + ll];
          }
        }
        output[ii * inputShape[0] + jj] = tmp;
      }
    }
  }

  float getResult(int i, int j) { return output[i * inputShape[0] + j]; }
};
