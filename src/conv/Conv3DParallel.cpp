#include "../Benchmarkable.cpp"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"

class Conv3DParallel : public Benchmarkable {
private:
  float *input;
  float *output;
  float *kernel;

  int *inputShape;
  int *kernelShape;
  int kernelRadius[3];

public:
  Conv3DParallel(float *input, float *output, float *kernel, int *inputShape,
                 int *kernelShape)
      : input(input), output(output), kernel(kernel), inputShape(inputShape),
        kernelShape(kernelShape) {
    kernelRadius[0] = kernelShape[0] / 2;
    kernelRadius[1] = kernelShape[1] / 2;
    kernelRadius[2] = kernelShape[2] / 2;
  }

  ~Conv3DParallel() {}

  void run() {
    auto convolve = [=](const tbb::blocked_range2d<int> &r) {
      for (int ii = r.rows().begin(); ii < r.rows().end(); ii++) {
        for (int jj = r.cols().begin(); jj < r.cols().end(); jj++) {
          for (int kk = kernelRadius[2]; kk < inputShape[2] - kernelRadius[2];
               kk++) {
            // [ii, jj, kk] is the position in the output image
            float tmp = 0;
            for (int ll = 0; ll < kernelShape[0]; ll++) {
              for (int mm = 0; mm < kernelShape[1]; mm++) {
                for (int nn = 0; nn < kernelShape[2]; nn++) {
                  // [ll, mm, nn] is  the position in the kernel
                  auto image =
                      input[(ii - kernelRadius[0] + ll) * inputShape[0] *
                                inputShape[1] +
                            (jj - kernelRadius[1] + mm) * inputShape[1] +
                            (kk - kernelRadius[2] + nn)];
                  auto filter = kernel[ll * kernelShape[0] * kernelShape[1] +
                                       mm * kernelShape[1] + nn];
                  tmp += image * filter;
                }
              }
            }
            output[ii * inputShape[0] * inputShape[1] + jj * inputShape[1] +
                   kk] = tmp;
          }
        }
      }
    };

    // Check if environment variables are set
    const char *gsx = std::getenv("GRAIN_SIZE_X");
    const char *gsy = std::getenv("GRAIN_SIZE_Y");

    // Use auto partioner by default
    auto range = tbb::blocked_range2d<int>(
        kernelRadius[0], inputShape[0] - kernelRadius[0], kernelRadius[1],
        inputShape[1] - kernelRadius[1]);

    // Unless grain size was set
    if (gsx && gsy) {
      range = tbb::blocked_range2d<int>(
          kernelRadius[0], inputShape[0] - kernelRadius[0], std::stoi(gsx),
          kernelRadius[1], inputShape[1] - kernelRadius[1], std::stoi(gsy));
    }

    tbb::parallel_for(range, convolve);
  }

  float getResult(int i, int j, int k) {
    return output[i * inputShape[0] * inputShape[1] + j * inputShape[1] + k];
  }
};
