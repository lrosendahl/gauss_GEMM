#include "../Benchmarkable.cpp"

class Conv3DSerial : public Benchmarkable {
private:
  float *input;
  float *output;
  float *kernel;

  int *inputShape;
  int *kernelShape;
  int kernelRadius[3];

public:
  Conv3DSerial(float *input, float *output, float *kernel, int *inputShape,
               int *kernelShape)
      : input(input), output(output), kernel(kernel), inputShape(inputShape),
        kernelShape(kernelShape) {
    kernelRadius[0] = kernelShape[0] / 2;
    kernelRadius[1] = kernelShape[1] / 2;
    kernelRadius[2] = kernelShape[2] / 2;
  }

  ~Conv3DSerial() {}

  void run() {
    for (int ii = kernelRadius[0]; ii < inputShape[0] - kernelRadius[0]; ii++) {
      for (int jj = kernelRadius[1]; jj < inputShape[1] - kernelRadius[1];
           jj++) {
        for (int kk = kernelRadius[2]; kk < inputShape[2] - kernelRadius[2];
             kk++) {
          // [ii, jj, kk] is the position in the output image
          float tmp = 0;
          for (int ll = 0; ll < kernelShape[0]; ll++) {
            for (int mm = 0; mm < kernelShape[1]; mm++) {
              for (int nn = 0; nn < kernelShape[2]; nn++) {
                // [ll, mm, nn] is  the position in the kernel
                auto image = input[(ii - kernelRadius[0] + ll) * inputShape[0] *
                                       inputShape[1] +
                                   (jj - kernelRadius[1] + mm) * inputShape[1] +
                                   (kk - kernelRadius[2] + nn)];
                auto filter = kernel[ll * kernelShape[0] * kernelShape[1] +
                                     mm * kernelShape[1] + nn];
                tmp += image * filter;
              }
            }
          }
          output[ii * inputShape[0] * inputShape[1] + jj * inputShape[1] + kk] =
              tmp;
        }
      }
    }
  }

  float getResult(int i, int j, int k) {
    return output[i * inputShape[0] * inputShape[1] + j * inputShape[1] + k];
  }
};
