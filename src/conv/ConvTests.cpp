#define BOOST_TEST_DYN_LINK

#include "Conv2DParallel.cpp"
#include "Conv2DSerial.cpp"
#include "Conv3DParallel.cpp"
#include "Conv3DSerial.cpp"
#include <boost/test/unit_test.hpp>

#define EPSILON (float)0.001

namespace conv {

// Used to initialize image
float initImage2D(int i, int j) { return i * j + 1; }
float initImage3D(int i, int j, int k) { return i * j * k + 1; }

// Gauss kernel with parameters size=3 sigma=0.391 normalized
float gaussKernel[] = {0.00124641, 0.0328117,  0.00124641, 0.0328117, 0.863767,
                       0.0328117,  0.00124641, 0.0328117,  0.00124641};

// 3D Jacobi Kernel
float jacobiKernel[] = {0, 0,     0, 0,     0.165, 0,     0, 0,     0,
                        0, 0.164, 0, 0.162, -1.67, 0.161, 0, 0.163, 0,
                        0, 0,     0, 0,     0.166, 0,     0, 0,     0};

float identity3D[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// Expected output image (computed using SciPy)
float expected2DGauss[] = {
    0,         0,         0,         0,         0,         0,         0,
    0,         0,         0,         0,         0,         0,         0,
    0,         0,         0,         2.0,       3.0,       4.0,       5.0,
    6.0,       7.0,       8.0,       8.99999,   9.99999,   10.99999,  11.99999,
    12.99999,  13.99999,  14.99999,  0,         0,         3.0,       5.0,
    7.0,       8.99999,   10.99999,  12.99999,  14.99999,  16.99999,  18.99999,
    20.99999,  22.99999,  24.99999,  26.99998,  28.99998,  0,         0,
    4.0,       7.0,       9.99999,   12.99999,  15.99999,  18.99999,  21.99999,
    24.99999,  27.99998,  30.99998,  33.99998,  36.99998,  39.99998,  42.99998,
    0,         0,         5.0,       8.99999,   12.99999,  16.99999,  20.99999,
    24.99999,  28.99998,  32.99998,  36.99998,  40.99998,  44.99997,  48.99997,
    52.99997,  56.99997,  0,         0,         6.0,       10.99999,  15.99999,
    20.99999,  25.99999,  30.99998,  35.99998,  40.99998,  45.99997,  50.99997,
    55.99997,  60.99997,  65.99996,  70.99996,  0,         0,         7.0,
    12.99999,  18.99999,  24.99999,  30.99998,  36.99998,  42.99998,  48.99997,
    54.99997,  60.99997,  66.99996,  72.99996,  78.99996,  84.99995,  0,
    0,         8.0,       14.99999,  21.99999,  28.99998,  35.99998,  42.99998,
    49.99997,  56.99997,  63.99996,  70.99996,  77.99996,  84.99995,  91.99995,
    98.99994,  0,         0,         8.99999,   16.99999,  24.99999,  32.99998,
    40.99998,  48.99997,  56.99997,  64.99996,  72.99996,  80.99995,  88.99995,
    96.99995,  104.99994, 112.99994, 0,         0,         9.99999,   18.99999,
    27.99998,  36.99998,  45.99997,  54.99997,  63.99996,  72.99996,  81.99995,
    90.99995,  99.99994,  108.99994, 117.99993, 126.99993, 0,         0,
    10.99999,  20.99999,  30.99998,  40.99998,  50.99997,  60.99997,  70.99996,
    80.99995,  90.99995,  100.99994, 110.99994, 120.99993, 130.99993, 140.99992,
    0,         0,         11.99999,  22.99999,  33.99998,  44.99997,  55.99997,
    66.99996,  77.99996,  88.99995,  99.99994,  110.99994, 121.99993, 132.99993,
    143.99992, 154.99991, 0,         0,         12.99999,  24.99999,  36.99998,
    48.99997,  60.99997,  72.99996,  84.99995,  96.99995,  108.99994, 120.99993,
    132.99993, 144.99992, 156.99991, 168.99991, 0,         0,         13.99999,
    26.99998,  39.99998,  52.99997,  65.99996,  78.99996,  91.99995,  104.99994,
    117.99993, 130.99993, 143.99992, 156.99991, 169.9999,  182.9999,  0,
    0,         14.99999,  28.99998,  42.99998,  56.99997,  70.99996,  84.99995,
    98.99994,  112.99994, 126.99993, 140.99992, 154.99991, 168.99991, 182.9999,
    196.99989, 0,         0,         0,         0,         0,         0,
    0,         0,         0,         0,         0,         0,         0,
    0,         0,         0,         0};

// This is for i=0, j=0, for all other values of i and j the values are the same
float expected3DJacobi[] = {-1.379, -2.068, -2.757, -3.446,
                            -4.135, -4.824, -5.513, -6.202};

BOOST_AUTO_TEST_CASE(conv_2D_serial_test) {
  int inputShape[] = {16, 16};
  int kernelShape[] = {3, 3};

  float *input = new float[inputShape[0] * inputShape[1]];
  float *output = new float[inputShape[0] * inputShape[1]];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      input[i * inputShape[0] + j] = initImage2D(i, j);
      output[i * inputShape[0] + j] = 0;
    }
  }

  Conv2DSerial conv2DSerial(input, output, gaussKernel, inputShape,
                            kernelShape);
  conv2DSerial.run();

  // Test every value of the output against expected results
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      BOOST_TEST(conv2DSerial.getResult(i, j) ==
                     expected2DGauss[i * inputShape[0] + j],
                 boost::test_tools::tolerance(EPSILON));
    }
  }

  delete input;
  delete output;
}

BOOST_AUTO_TEST_CASE(conv_2D_parallel_test) {
  int inputShape[] = {16, 16};
  int kernelShape[] = {3, 3};

  float *input = new float[inputShape[0] * inputShape[1]];
  float *output = new float[inputShape[0] * inputShape[1]];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      input[i * inputShape[0] + j] = initImage2D(i, j);
      output[i * inputShape[0] + j] = 0;
    }
  }

  Conv2DParallel conv2DParallel(input, output, gaussKernel, inputShape,
                                kernelShape);
  conv2DParallel.run();

  // Test every value of the output against expected results
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      BOOST_TEST(conv2DParallel.getResult(i, j) ==
                     expected2DGauss[i * inputShape[0] + j],
                 boost::test_tools::tolerance(EPSILON));
    }
  }

  delete input;
  delete output;
}

BOOST_AUTO_TEST_CASE(conv_3D_serial_identity_test) {
  int inputShape[] = {16, 16, 16};
  int kernelShape[] = {3, 3, 3};

  float *input = new float[inputShape[0] * inputShape[1] * inputShape[2]];
  float *output = new float[inputShape[0] * inputShape[1] * inputShape[2]];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      for (int k = 0; k < inputShape[2]; k++) {
        input[i * inputShape[0] * inputShape[1] + j * inputShape[1] + k] =
            initImage3D(i, j, k);
        output[i * inputShape[0] * inputShape[1] + j * inputShape[1] + k] = 0;
      }
    }
  }

  Conv3DSerial conv3DSerial(input, output, identity3D, inputShape, kernelShape);
  conv3DSerial.run();

  // Test every value of the output against expected results
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      for (int k = 0; k < inputShape[2]; k++) {
        if (i == 0 || i == 15 || j == 0 || j == 15 || k == 0 || k == 15) {
          BOOST_TEST(conv3DSerial.getResult(i, j, k) == 0,
                     boost::test_tools::tolerance(EPSILON));
        } else {
          BOOST_TEST(conv3DSerial.getResult(i, j, k) ==
                         input[i * inputShape[0] * inputShape[1] +
                               j * inputShape[1] + k],
                     boost::test_tools::tolerance(EPSILON));
        }
      }
    }
  }

  delete input;
  delete output;
}

BOOST_AUTO_TEST_CASE(conv_3D_serial_jacobi_test) {
  int inputShape[] = {10, 10, 10};
  int kernelShape[] = {3, 3, 3};

  float *input = new float[inputShape[0] * inputShape[1] * inputShape[2]];
  float *output = new float[inputShape[0] * inputShape[1] * inputShape[2]];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0] * inputShape[1] * inputShape[2]; i++) {
    input[i] = (i % 10) + 1;
    output[i] = 0;
  }

  Conv3DSerial conv3DSerial(input, output, jacobiKernel, inputShape,
                            kernelShape);
  conv3DSerial.run();

  // Test every value of the output against expected results
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      for (int k = 0; k < inputShape[2]; k++) {
        auto e = expected3DJacobi[k - 1];
        if (i == 0 || i == 9 || j == 0 || j == 9 || k == 0 || k == 9) {
          e = 0;
        }
        BOOST_TEST(conv3DSerial.getResult(i, j, k) == e,
                   boost::test_tools::tolerance(EPSILON));
      }
    }
  }

  delete input;
  delete output;
}

BOOST_AUTO_TEST_CASE(conv_3D_parallel_identity_test) {
  int inputShape[] = {16, 16, 16};
  int kernelShape[] = {3, 3, 3};

  float *input = new float[inputShape[0] * inputShape[1] * inputShape[2]];
  float *output = new float[inputShape[0] * inputShape[1] * inputShape[2]];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      for (int k = 0; k < inputShape[2]; k++) {
        input[i * inputShape[0] * inputShape[1] + j * inputShape[1] + k] =
            initImage3D(i, j, k);
        output[i * inputShape[0] * inputShape[1] + j * inputShape[1] + k] = 0;
      }
    }
  }

  Conv3DParallel conv3DParallel(input, output, identity3D, inputShape,
                                kernelShape);
  conv3DParallel.run();

  // Test every value of the output against expected results
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      for (int k = 0; k < inputShape[2]; k++) {
        if (i == 0 || i == 15 || j == 0 || j == 15 || k == 0 || k == 15) {
          BOOST_TEST(conv3DParallel.getResult(i, j, k) == 0,
                     boost::test_tools::tolerance(EPSILON));
        } else {
          BOOST_TEST(conv3DParallel.getResult(i, j, k) ==
                         input[i * inputShape[0] * inputShape[1] +
                               j * inputShape[1] + k],
                     boost::test_tools::tolerance(EPSILON));
        }
      }
    }
  }

  delete input;
  delete output;
}

BOOST_AUTO_TEST_CASE(conv_3D_parallel_jacobi_test) {
  int inputShape[] = {10, 10, 10};
  int kernelShape[] = {3, 3, 3};

  float *input = new float[inputShape[0] * inputShape[1] * inputShape[2]];
  float *output = new float[inputShape[0] * inputShape[1] * inputShape[2]];

  // Initialize input and output images
  for (int i = 0; i < inputShape[0] * inputShape[1] * inputShape[2]; i++) {
    input[i] = (i % 10) + 1;
    output[i] = 0;
  }

  Conv3DParallel conv3DParallel(input, output, jacobiKernel, inputShape,
                                kernelShape);
  conv3DParallel.run();

  // Test every value of the output against expected results
  for (int i = 0; i < inputShape[0]; i++) {
    for (int j = 0; j < inputShape[1]; j++) {
      for (int k = 0; k < inputShape[2]; k++) {
        auto e = expected3DJacobi[k - 1];
        if (i == 0 || i == 9 || j == 0 || j == 9 || k == 0 || k == 9) {
          e = 0;
        }
        BOOST_TEST(conv3DParallel.getResult(i, j, k) == e,
                   boost::test_tools::tolerance(EPSILON));
      }
    }
  }

  delete input;
  delete output;
}
}
