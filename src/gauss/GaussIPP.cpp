#include "../Benchmarkable.cpp"
#include "ipp.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <stdio.h>

class GaussIPP : public Benchmarkable {
private:
  int width;
  int height;
  IppStatus status = ippStsNoErr;
  Ipp32f *pSrc = NULL, *pDst = NULL;
  int srcStep = 1, dstStep = 0;
  IppiSize roiSize;
  Ipp32u kernelSize = 3;
  Ipp32f sigma = 0.391f;
  Ipp8u *pBuffer = NULL;
  IppFilterGaussianSpec *pSpec = NULL;
  int iTmpBufSize = 0, iSpecSize = 0;
  IppiBorderType borderType = ippBorderConst;
  Ipp8u borderValue = 0;
  int numChannels = 1;

public:
  GaussIPP(int width, int height, float (*initImage)(int, int))
      : width(width), height(height) {

    IppiSize roiSize = {width, height};
    this->roiSize = roiSize;

    // allocate 64bit aligned memory
    pSrc = ippiMalloc_32f_C1(roiSize.width, roiSize.height, &srcStep);
    pDst = ippiMalloc_32f_C1(roiSize.width, roiSize.height, &dstStep);

    // setup values
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        pSrc[i * srcStep / 4 + j] = initImage(i, j);
      }
    }

    ippiFilterGaussianGetBufferSize(roiSize, kernelSize, ipp32f, numChannels,
                                    &iSpecSize, &iTmpBufSize);

    pSpec = (IppFilterGaussianSpec *)ippsMalloc_8u(iSpecSize);
    pBuffer = ippsMalloc_8u(iTmpBufSize);

    // init Gauss kernel with given parameters
    ippiFilterGaussianInit(roiSize, kernelSize, sigma, borderType, ipp32f,
                           numChannels, pSpec, pBuffer);
  }

  ~GaussIPP() {
    ippsFree(pBuffer);
    ippsFree(pSpec);
    ippiFree(pSrc);
    ippiFree(pDst);
  }

  void run() {
    // apply Gauss filter
    ippiFilterGaussianBorder_32f_C1R(pSrc, srcStep, pDst, dstStep, roiSize,
                                     borderValue, pSpec, pBuffer);
  };

  float getResult(int i, int j) { return pDst[i * dstStep / 4 + j]; }
};
