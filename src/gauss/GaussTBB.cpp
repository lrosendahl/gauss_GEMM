#include "../Benchmarkable.cpp"
#include "tbb/blocked_range.h"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <math.h>

// Gauss parameters
// #define KERNEL_SIZE 3
// #define SIGMA 0.391
#define KERNEL_SIZE 91
#define SIGMA 15

class GaussTBB : public Benchmarkable {
private:
  int width;
  int height;

  // derived parameters
  int radius = KERNEL_SIZE / 2;

  float *a;
  float *blurred_a;
  float kernel[KERNEL_SIZE];

public:
  GaussTBB(int width, int height, float (*initImage)(int, int))
      : width(width), height(height) {

    a = new float[width * height];
    blurred_a = new float[width * height];

    // Setup image
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        a[i * height + j] = initImage(i, j);
      }
    }

    // calculate Kernel
    calcKernel();
  }

  ~GaussTBB() {
    delete[] a;
    delete[] blurred_a;
  }

  void run() {
    auto gaussianBlurXBody = [=](const tbb::blocked_range2d<int> &r) {
      for (int i = r.rows().begin(); i != r.rows().end(); ++i) {
        for (int j = r.cols().begin(); j != r.cols().end(); ++j) {
          float sum = 0;
          for (int k = 0; k < KERNEL_SIZE; ++k) {
            // if (!((j - radius + k) < 0 || (j - radius + k) > (height - 1)))
            sum += a[i * height + j - radius + k] * kernel[k];
          }
          blurred_a[i * height + j] = sum;
        }
      }
    };

    auto gaussianBlurXEdgeBody = [=](const tbb::blocked_range<int> &r) {
      // rows with missing entries
      for (int i = r.begin(); i != r.end(); ++i) {
        // columns with missing entries
        for (int j1 = 0; j1 < radius; j1++) {
          float sum1 = 0;
          float sum2 = 0;
          int j2 = (height - 1 - j1);
          for (int k = 0; k < KERNEL_SIZE; ++k) {
            if (!((j1 - radius + k) < 0))
              sum1 += a[i * height + (j1 - radius + k)] * kernel[k];
            if (!((j2 - radius + k) > (height - 1)))
              sum2 += a[i * height + (j2 - radius + k)] * kernel[k];
          }
          blurred_a[i * height + j1] = sum1;
          blurred_a[i * height + j2] = sum2;
        }
      }
    };

    auto gaussianBlurYBody = [=](const tbb::blocked_range2d<int> &r) {
      for (int i = r.rows().begin(); i != r.rows().end(); ++i) {
        for (int j = r.cols().begin(); j != r.cols().end(); ++j) {
          float sum = 0;
          for (int k = 0; k < KERNEL_SIZE; ++k) {
            // if (!((i - radius + k) < 0 || (i - radius + k) > (height - 1)))
            sum += blurred_a[(i - radius + k) * height + j] * kernel[k];
          }
          a[i * height + j] = sum;
        }
      }
    };

    auto gaussianBlurYEdgeBody = [=](const tbb::blocked_range<int> &r) {
      // rows with missing entries
      for (int j = r.begin(); j != r.end(); ++j) {
        // columns with missing entries
        for (int i1 = 0; i1 < radius; i1++) {
          float sum1 = 0;
          float sum2 = 0;
          int i2 = (width - 1 - i1);
          for (int k = 0; k < KERNEL_SIZE; ++k) {
            if (!((i1 - radius + k) < 0))
              sum1 += blurred_a[(i1 - radius + k) * height + j] * kernel[k];
            if (!((i2 - radius + k) > (height - 1)))
              sum2 += blurred_a[(i2 - radius + k) * height + j] * kernel[k];
          }
          a[i1 * height + j] = sum1;
          a[i2 * height + j] = sum2;
        }
      }
    };

    // X Convolution
    parallel_for(tbb::blocked_range2d<int>(0, width, radius, height - radius),
                 gaussianBlurXBody);

    // X Convolution edges
    parallel_for(tbb::blocked_range<int>(0, width, 16), gaussianBlurXEdgeBody);

    // Y Convolution
    parallel_for(tbb::blocked_range2d<int>(radius, width - radius, 0, height),
                 gaussianBlurYBody);

    // Y Convolution edges
    parallel_for(tbb::blocked_range<int>(0, height, 16), gaussianBlurYEdgeBody);
  }

  float getResult(int i, int j) { return a[i * width + j]; }

  void calcKernel() {
    // Calculate Gauss Distribution
    float factor = 1 / (sqrt(2 * M_PI) * SIGMA);
    float total = 0;

    // Set 1D Kernel
    for (int i = -radius; i <= radius; i++) {
      float value = exp(-(i * i) / (2 * SIGMA * SIGMA));
      value = factor * value;
      total += value;
      kernel[i + radius] = value;
    }

    // Set 1D nomalized Kernel
    for (int i = 0; i < KERNEL_SIZE; i++) {
      kernel[i] = kernel[i] / total;
    }
  }
};
