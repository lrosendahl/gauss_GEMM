#include "../Benchmarkable.cpp"
#include "ipp.h"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include "tbb/tick_count.h"
#include <iostream>
#include <math.h>
#include <stdio.h>

// Gauss parameters
#define KERNEL_SIZE 91
#define SIGMA 15

class GaussTBBIPP : public Benchmarkable {
private:
  int width;
  int height;
  int roiX = 1000;
  int roiY = 1000;

  IppStatus status = ippStsNoErr;
  Ipp32f *pSrc = NULL, *pDst = NULL;
  int srcStep = 1, dstStep = 0;
  IppiSize imageSize;
  IppiSize roiSize = {roiX, roiY};
  int radius;
  int rad2;
  Ipp8u borderValue = 0;
  int numChannels = 1;

public:
  GaussTBBIPP(int width, int height, float (*initImage)(int, int))
      : width(width), height(height) {
    IppiSize imageSize = {width, height};
    this->imageSize = imageSize;
    radius = ceil(KERNEL_SIZE / 2.0);
    rad2 = KERNEL_SIZE / 2;

    pSrc = ippiMalloc_32f_C1(imageSize.width + 2 * rad2,
                             imageSize.height + 2 * rad2, &srcStep);
    pDst = ippiMalloc_32f_C1(imageSize.width + 2 * rad2,
                             imageSize.height + 2 * rad2, &dstStep);

    // setup values for whole image
    for (int i = rad2; i < height + rad2; i++) {
      for (int j = rad2; j < width + rad2; j++) {
        pSrc[i * srcStep / 4 + j] = initImage(i - rad2, j - rad2);
      }
    }
  }

  ~GaussTBBIPP() {
    ippiFree(pSrc);
    ippiFree(pDst);
  }

  // Process a single ROI of an image
  void processROI(IppiSize roiSize, Ipp32f *pSrcRoi, Ipp32f *pDstRoi,
                  int srcStep, int dstStep,
                  IppiBorderType borderType = ippBorderInMem) {

    // initialize required parameters
    Ipp8u *pBuffer = NULL;               /* Pointer to the work buffer */
    IppFilterGaussianSpec *pSpec = NULL; /* context structure */
    int iTmpBufSize = 0, iSpecSize = 0;  /* Common work buffer size */

    ippiFilterGaussianGetBufferSize(roiSize, KERNEL_SIZE, ipp32f, numChannels,
                                    &iSpecSize, &iTmpBufSize);

    pSpec = (IppFilterGaussianSpec *)ippsMalloc_8u(iSpecSize);
    pBuffer = ippsMalloc_8u(iTmpBufSize);

    // init Gauss kernel with given parameters
    ippiFilterGaussianInit(roiSize, KERNEL_SIZE, SIGMA, borderType, ipp32f,
                           numChannels, pSpec, pBuffer);

    // apply Gauss filter
    ippiFilterGaussianBorder_32f_C1R(pSrcRoi, srcStep, pDstRoi, dstStep,
                                     roiSize, borderValue, pSpec, pBuffer);

    // free memory of temporary buffers
    ippsFree(pBuffer);
    ippsFree(pSpec);
  }

  void run() {
    // run parallel_for loop
    parallel_for(
        tbb::blocked_range2d<int>(0, ceil(height / (float)roiY), 0,
                                  ceil(width / (float)roiX)),
        [=](const tbb::blocked_range2d<int> &r) {
          for (int i = r.rows().begin(); i != r.rows().end(); ++i) {
            for (int j = r.cols().begin(); j != r.cols().end(); ++j) {

              Ipp32f *pSrcRoi =
                  &pSrc[((i * roiY) + rad2) * srcStep / 4 + (j * roiX) + rad2];
              Ipp32f *pDstRoi =
                  &pDst[((i * roiY) + rad2) * dstStep / 4 + (j * roiX) + rad2];

              int altY =
                  ((i + 1) * roiY > height) ? (height - i * roiY) : (roiY);
              int altX = ((j + 1) * roiX > width) ? (width - j * roiX) : (roiX);

              // process a single ROI of size altX * altY
              processROI({altX, altY}, pSrcRoi, pDstRoi, srcStep, dstStep);
            }
          }
        });
  }

  float getResult(int i, int j) {
    return pDst[(i + rad2) * dstStep / 4 + (j + rad2)];
  }
};
