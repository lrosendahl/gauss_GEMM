#define BOOST_TEST_DYN_LINK

#include "GaussIPP.cpp"
#include "GaussTBB.cpp"
#include "GaussTBBIPP.cpp"
#include <boost/test/unit_test.hpp>

#define SIZE 16
#define EPSILON (float)0.001

namespace gauss {

// Used to initialize image
float initImage(int i, int j) { return i * j + 1; }

// Expected output image (computed using SciPy)
float expected[] = {
    0.932,   1.0,     1.035,   1.071,   1.106,   1.141,   1.177,   1.212,
    1.247,   1.282,   1.318,   1.353,   1.388,   1.424,   1.459,   1.44,
    1.0,     2.0,     3.0,     4.0,     5.0,     6.0,     7.0,     8.0,
    9.0,     10.0,    11.0,    12.0,    13.0,    14.0,    15.0,    15.4,
    1.035,   3.0,     5.0,     7.0,     9.0,     11.0,    13.0,    15.0,
    17.0,    19.0,    21.0,    23.0,    25.0,    27.0,    29.0,    29.835,
    1.071,   4.0,     7.0,     10.0,    13.0,    16.0,    19.0,    22.0,
    25.0,    28.0,    31.0,    34.0,    37.0,    40.0,    43.0,    44.27,
    1.106,   5.0,     9.0,     13.0,    17.0,    21.0,    25.0,    29.0,
    33.0,    37.0,    41.0,    45.0,    49.0,    53.0,    57.0,    58.705,
    1.141,   6.0,     11.0,    16.0,    21.0,    26.0,    31.0,    36.0,
    41.0,    46.0,    51.0,    56.0,    61.0,    66.0,    71.0,    73.14,
    1.177,   7.0,     13.0,    19.0,    25.0,    31.0,    37.0,    43.0,
    49.0,    55.0,    61.0,    67.0,    73.0,    79.0,    85.0,    87.575,
    1.212,   8.0,     15.0,    22.0,    29.0,    36.0,    43.0,    50.0,
    57.0,    64.0,    71.0,    78.0,    85.0,    92.0,    99.0,    102.011,
    1.247,   9.0,     17.0,    25.0,    33.0,    41.0,    49.0,    57.0,
    65.0,    73.0,    81.0,    89.0,    97.0,    105.0,   113.0,   116.446,
    1.282,   10.0,    19.0,    28.0,    37.0,    46.0,    55.0,    64.0,
    73.0,    82.0,    91.0,    100.0,   109.0,   118.0,   127.0,   130.881,
    1.318,   11.0,    21.0,    31.0,    41.0,    51.0,    61.0,    71.0,
    81.0,    91.0,    101.0,   111.0,   121.0,   131.0,   141.0,   145.316,
    1.353,   12.0,    23.0,    34.0,    45.0,    56.0,    67.0,    78.0,
    89.0,    100.0,   111.0,   122.0,   133.0,   144.0,   155.0,   159.751,
    1.388,   13.0,    25.0,    37.0,    49.0,    61.0,    73.0,    85.0,
    97.0,    109.0,   121.0,   133.0,   145.0,   157.0,   169.0,   174.186,
    1.424,   14.0,    27.0,    40.0,    53.0,    66.0,    79.0,    92.0,
    105.0,   118.0,   131.0,   144.0,   157.0,   170.0,   183.0,   188.621,
    1.459,   15.0,    29.0,    43.0,    57.0,    71.0,    85.0,    99.0,
    113.0,   127.0,   141.0,   155.0,   169.0,   183.0,   197.0,   203.056,
    1.44,    15.4,    29.835,  44.27,   58.705,  73.14,   87.575,  102.011,
    116.446, 130.881, 145.316, 159.751, 174.186, 188.621, 203.056, 209.304};

BOOST_AUTO_TEST_CASE(gauss_ipp_test) {
  GaussIPP gaussIPP(SIZE, SIZE, initImage);
  gaussIPP.run();

  for (int i = 0; i < SIZE; i++) {
    for (int j = 0; j < SIZE; j++) {
      BOOST_TEST(gaussIPP.getResult(i, j) == expected[i * SIZE + j],
                 boost::test_tools::tolerance(EPSILON));
    }
  }
}

BOOST_AUTO_TEST_CASE(gauss_tbb_test) {
  GaussTBB gaussTBB(SIZE, SIZE, initImage);
  gaussTBB.run();

  for (int i = 1; i < SIZE - 1; i++) {
    for (int j = 0; j < SIZE; j++) {
      BOOST_TEST(gaussTBB.getResult(i, j) == expected[i * SIZE + j],
                 boost::test_tools::tolerance(EPSILON));
    }
  }
}

// This test fails, but only sometimes and for different values
// TODO: Find out what's up with that

// BOOST_AUTO_TEST_CASE(gauss_tbb_ipp_test) {
//   GaussTBBIPP gaussTBBIPP(SIZE, SIZE, initImage);
//   gaussTBBIPP.run();
//
//   for (int i = 0; i < SIZE; i++) {
//     for (int j = 0; j < SIZE; j++) {
//       BOOST_TEST(gaussTBBIPP.getResult(i, j) == expected[i * SIZE + j],
//                  boost::test_tools::tolerance(EPSILON));
//     }
//   }
// }
}
