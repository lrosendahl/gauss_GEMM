#include "../Benchmarkable.cpp"
#include "mkl.h"
#include <stdio.h>

class GemmMKL : public Benchmarkable {
private:
  int m;
  int k;
  int n;
  const float alpha = 1.0;
  const float beta = 0.0;
  float *a;
  float *b;
  float *c;

public:
  GemmMKL(int m, int k, int n, float (*initA)(int, int),
          float (*initB)(int, int))
      : m(m), k(k), n(n) {
    a = (float *)mkl_malloc(m * k * sizeof(float), 64);
    b = (float *)mkl_malloc(k * n * sizeof(float), 64);
    c = (float *)mkl_malloc(m * n * sizeof(float), 64);

    if (a == NULL || b == NULL || c == NULL) {
      printf("ERROR: Failed to allocate memory for matrices. Aborting... \n");
      mkl_free(a);
      mkl_free(b);
      mkl_free(c);
    }

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < k; j++) {
        a[i * k + j] = initA(i, j);
      }
    }

    for (int i = 0; i < k; i++) {
      for (int j = 0; j < n; j++) {
        b[i * n + j] = initB(i, j);
      }
    }
  }

  ~GemmMKL() {
    mkl_free(a);
    mkl_free(b);
    mkl_free(c);
  }

  void run() {
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, a, k,
                b, n, beta, c, n);
  }

  float getResult(int i, int j) { return c[i * n + j]; }
};
