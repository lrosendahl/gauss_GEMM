#include "../Benchmarkable.cpp"

namespace gemmNaive {

class GemmNaive : public Benchmarkable {
private:
  int m;
  int n;
  int k;

  float *a;
  float *b;
  float *c;

public:
  GemmNaive(int m, int k, int n, float (*initA)(int, int),
            float (*initB)(int, int))
      : m(m), k(k), n(n) {
    a = new float[m * k];
    b = new float[k * n];
    c = new float[m * n];

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < k; j++) {
        a[i * k + j] = initA(i, j);
      }
    }

    for (int i = 0; i < k; i++) {
      for (int j = 0; j < n; j++) {
        b[i * n + j] = initB(i, j);
      }
    }

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        c[i * n + j] = 0;
      }
    }
  }

  ~GemmNaive() {
    delete[] a;
    delete[] b;
    delete[] c;
  }

  void run() {
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        float sum = 0;
        for (int l = 0; l < k; l++) {
          sum += a[i * k + l] * b[l * n + j];
        }
        c[i * n + j] = sum;
      }
    }
  }

  float getResult(int i, int j) { return c[i * n + j]; }
};
}
