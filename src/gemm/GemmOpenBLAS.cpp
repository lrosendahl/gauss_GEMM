#include "../Benchmarkable.cpp"
#include <cblas.h>
#include <stdio.h>

class GemmOpenBLAS : public Benchmarkable {
private:
  int m;
  int k;
  int n;

  float *a;
  float *b;
  float *c;

  const float alpha = 1.0;
  const float beta = 0.0;

public:
  GemmOpenBLAS(int m, int k, int n, float (*initA)(int, int),
               float (*initB)(int, int))
      : m(m), k(k), n(n) {
    a = new float[m * k];
    b = new float[k * n];
    c = new float[m * n];

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < k; j++) {
        a[i * k + j] = initA(i, j);
      }
    }

    for (int i = 0; i < k; i++) {
      for (int j = 0; j < n; j++) {
        b[i * n + j] = initB(i, j);
      }
    }
  }

  ~GemmOpenBLAS() {
    delete[](a);
    delete[](b);
    delete[](c);
  }

  void run() {
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, a, k,
                b, n, beta, c, n);
  }

  float getResult(int i, int j) { return c[i * n + j]; }
};
