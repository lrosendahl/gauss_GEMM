#include "../Benchmarkable.cpp"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include <stdio.h>

//Set const parameters to default values ifndef
#ifndef RANGEX
#define RANGEX 32
#endif

#ifndef RANGEY
#define RANGEY 32
#endif

class GemmTBBv1 : public Benchmarkable {
private:
  int m;
  int k;
  int n;
  float *__restrict__ a;
  float *__restrict__ b;
  float *__restrict__ bT;
  float *__restrict__ c;

public:
  GemmTBBv1(int m, int k, int n, float (*initA)(int, int),
            float (*initB)(int, int))
      : m(m), k(k), n(n) {

    a = new float[m * k];
    b = new float[k * n];
    bT = new float[n * k];
    c = new float[m * n];

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < k; j++) {
        a[i * k + j] = initA(i, j);
      }
    }

    for (int i = 0; i < k; i++) {
      for (int j = 0; j < n; j++) {
        b[i * n + j] = initB(i, j);
      }
    }
  };

  ~GemmTBBv1() {
    delete[] a;
    delete[] b;
    delete[] bT;
    delete[] c;
  }

  void run() {
    auto transpose = [=](const tbb::blocked_range2d<size_t> &r) {
      int rowStart = r.rows().begin();
      int rowEnd = r.rows().end();
      int colStart = r.cols().begin();
      int colEnd = r.cols().end();
      for (size_t i = rowStart; i != rowEnd; ++i) {
        float *bp = &b[i * n + colStart];
        for (size_t j = colStart; j != colEnd; ++j) {
          bT[j * k + i] = *(bp++);
        }
      }
    };

    auto multiply = [=](const tbb::blocked_range2d<size_t> &r) {
      int rowStart = r.rows().begin();
      int rowEnd = r.rows().end();
      int colStart = r.cols().begin();
      int colEnd = r.cols().end();
      for (size_t i = rowStart; i != rowEnd; ++i) {
        float *cp = &c[i * n + colStart];
        for (size_t j = colStart; j != colEnd; ++j) {
          float sum = 0;
          float *ap = &a[i * k];
          float *bp = &bT[j * k];
          for (size_t l = 0; l < k; ++l)
            sum += *(ap++) * *(bp++);
          *(cp++) = sum;
        }
      }
    };

    tbb::parallel_for(tbb::blocked_range2d<size_t>(0, k, RANGEX, 0, n, RANGEY),
                      transpose, tbb::simple_partitioner());
    tbb::parallel_for(tbb::blocked_range2d<size_t>(0, m, RANGEX, 0, n,RANGEY),
                      multiply, tbb::simple_partitioner());
  }

  float getResult(int i, int j) { return c[i * n + j]; }
};
