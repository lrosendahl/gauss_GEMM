#include "../Benchmarkable.cpp"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

/** Simple GEMM version using cache blocking and some manual loop-unrolling.
    Explicitly not transposing matrix B.
    Not yet fully adjusted for arbitrary input sizes**/

//Set const parameters to default values ifndef
#ifndef IBSIZE
#define IBSIZE 250
#endif

#ifndef KBSIZE
#define KBSIZE 250
#endif

class GemmTBBv2 : public Benchmarkable {
private:
  // Matrix dimensions
  int m;
  int k;
  int n;

  // Matrix pointers
  float *A;
  float *B;
  float *C;

public:
  GemmTBBv2(int m, int k, int n, float (*initA)(int, int),
            float (*initB)(int, int))
      : m(m), k(k), n(n) {

    A = new float[m * k];
    B = new float[k * n];
    C = new float[m * n];

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < k; j++) {
        A[i * k + j] = initA(i, j);
      }
    }

    for (int i = 0; i < k; i++) {
      for (int j = 0; j < n; j++) {
        B[i * n + j] = initB(i, j);
      }
    }
  };

  ~GemmTBBv2() {
    delete[] A;
    delete[] B;
    delete[] C;
  }

  void run() {
    auto multiply = [=](const tbb::blocked_range<size_t> &r) {
      for (int jj = IBSIZE * r.begin(); jj < IBSIZE * r.end(); jj += IBSIZE) {
        for (int kk = 0; kk < k; kk += KBSIZE) {
          for (int i = 0; i < m; i += 2) {
            for (int j = jj; j < jj + IBSIZE; j += 2) {
              float acc00 = 0, acc01 = 0, acc10 = 0, acc11 = 0;
              if (kk != 0) {
                acc00 = C[(i)*n + (j)];
                acc01 = C[(i)*n + (j + 1)];
                acc10 = C[(i + 1) * n + (j)];
                acc11 = C[(i + 1) * n + (j + 1)];
              }
              for (int _k = kk; _k < kk + KBSIZE; _k++) {
                acc00 += A[(i)*k + (_k)] * B[(_k)*n + (j)];
                acc01 += A[(i)*k + (_k)] * B[(_k)*n + (j + 1)];
                acc10 += A[(i + 1) * k + (_k)] * B[(_k)*n + (j)];
                acc11 += A[(i + 1) * k + (_k)] * B[(_k)*n + (j + 1)];
              }
              C[(i)*n + (j)] = acc00;
              C[(i)*n + (j + 1)] = acc01;
              C[(i + 1) * n + (j)] = acc10;
              C[(i + 1) * n + (j + 1)] = acc11;
            }
          }
        }
      }
    };

    static tbb::affinity_partitioner ap;
    tbb::parallel_for(tbb::blocked_range<size_t>(0, n / IBSIZE), multiply, ap);
  }

  float getResult(int i, int j) { return C[i * n + j]; }
};
