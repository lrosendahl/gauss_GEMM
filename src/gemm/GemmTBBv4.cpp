#include "../Benchmarkable.cpp"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include <stdio.h>

// sw03 (cost 70):
// M_L1_CB_SIZE = 128
// M_L2_CB_SIZE = 128
// N_L1_CB_SIZE = 8
// N_L2_CB_SIZE = 8
// K_L1_CB_SIZE = 16
// K_L2_CB_SIZE = 32
// L1_LOOP_ORDER = 3
// L2_LOOP_ORDER = 3

// sw03 (cost 65):
// M_L1_CB_SIZE = 64
// M_L2_CB_SIZE = 64
// N_L1_CB_SIZE = 8
// N_L2_CB_SIZE = 32
// K_L1_CB_SIZE = 16
// K_L2_CB_SIZE = 16
// L1_LOOP_ORDER = 1
// L2_LOOP_ORDER = 1

#ifndef M
#define M 1024
#endif

#ifndef N
#define N 1024
#endif

#ifndef K
#define K 1024
#endif

#ifndef M_L2_CB_SIZE
#define M_L2_CB_SIZE 256 // constraint: divides( M )
#endif

#ifndef M_L1_CB_SIZE
#define M_L1_CB_SIZE 128 // constraint: divides( M_L2_CB_SIZE )
#endif

#ifndef N_L2_CB_SIZE
#define N_L2_CB_SIZE 4 // constraint: divides( N )
#endif

#ifndef N_L1_CB_SIZE
#define N_L1_CB_SIZE 4 // constraint: divides( N_L2_CB_SIZE )
#endif

#ifndef K_L2_CB_SIZE
#define K_L2_CB_SIZE 64 // constraint: divides( K )
#endif

#ifndef K_L1_CB_SIZE
#define K_L1_CB_SIZE 32 // constraint: divides( K_L2_CB_SIZE )
#endif

#ifndef L2_LOOP_ORDER
#define L2_LOOP_ORDER 1 // range: {1,...,6}
#endif

#ifndef L1_LOOP_ORDER
#define L1_LOOP_ORDER 1 // range: {1,...,6}
#endif

namespace gemmTBBv4 {

#define A(i, j) a[(i)*K + (j)]
#define B(i, j) b[(i)*N + (j)]
#define C(i, j) c[(i)*N + (j)]

class Multiply {
private:
  float *a;
  float *b;
  float *c;

public:
  Multiply(float *a, float *b, float *c) : a(a), b(b), c(c){};

  void operator()(const tbb::blocked_range2d<int> &range) const {
    // void operator()() const {

    float sum = 0.f;
    auto rb = range.rows().begin();
    auto re = range.rows().end();
    auto cb = range.cols().begin();
    auto ce = range.cols().end();

// L2
#if L2_LOOP_ORDER == 1
    for (int iii = rb; iii < re; iii += M_L2_CB_SIZE) {
      for (int jjj = cb; jjj < ce; jjj += N_L2_CB_SIZE) {
        for (int kkk = 0; kkk < K; kkk += K_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 2
    for (int iii = rb; iii < re; iii += M_L2_CB_SIZE) {
      for (int kkk = 0; kkk < K; kkk += K_L2_CB_SIZE) {
        for (int jjj = cb; jjj < ce; jjj += N_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 3
    for (int jjj = cb; jjj < ce; jjj += N_L2_CB_SIZE) {
      for (int iii = rb; iii < re; iii += M_L2_CB_SIZE) {
        for (int kkk = 0; kkk < K; kkk += K_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 4
    for (int jjj = cb; jjj < ce; jjj += N_L2_CB_SIZE) {
      for (int kkk = 0; kkk < K; kkk += K_L2_CB_SIZE) {
        for (int iii = rb; iii < re; iii += M_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 5
    for (int kkk = 0; kkk < K; kkk += K_L2_CB_SIZE) {
      for (int iii = rb; iii < re; iii += M_L2_CB_SIZE) {
        for (int jjj = cb; jjj < ce; jjj += N_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 6
    for (int kkk = 0; kkk < K; kkk += K_L2_CB_SIZE) {
      for (int jjj = cb; jjj < ce; jjj += N_L2_CB_SIZE) {
        for (int iii = rb; iii < re; iii += M_L2_CB_SIZE) {
#endif

// L1
#if L1_LOOP_ORDER == 1
          for (int ii = iii; ii < iii + M_L2_CB_SIZE; ii += M_L1_CB_SIZE) {
            for (int jj = jjj; jj < jjj + N_L2_CB_SIZE; jj += N_L1_CB_SIZE) {
              for (int kk = kkk; kk < kkk + K_L2_CB_SIZE; kk += K_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 2
          for (int ii = iii; ii < iii + M_L2_CB_SIZE; ii += M_L1_CB_SIZE) {
            for (int kk = kkk; kk < kkk + K_L2_CB_SIZE; kk += K_L1_CB_SIZE) {
              for (int jj = jjj; jj < jjj + N_L2_CB_SIZE; jj += N_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 3
          for (int jj = jjj; jj < jjj + N_L2_CB_SIZE; jj += N_L1_CB_SIZE) {
            for (int ii = iii; ii < iii + M_L2_CB_SIZE; ii += M_L1_CB_SIZE) {
              for (int kk = kkk; kk < kkk + K_L2_CB_SIZE; kk += K_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 4
          for (int jj = jjj; jj < jjj + N_L2_CB_SIZE; jj += N_L1_CB_SIZE) {
            for (int kk = kkk; kk < kkk + K_L2_CB_SIZE; kk += K_L1_CB_SIZE) {
              for (int ii = iii; ii < iii + M_L2_CB_SIZE; ii += M_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 5
          for (int kk = kkk; kk < kkk + K_L2_CB_SIZE; kk += K_L1_CB_SIZE) {
            for (int ii = iii; ii < iii + M_L2_CB_SIZE; ii += M_L1_CB_SIZE) {
              for (int jj = jjj; jj < jjj + N_L2_CB_SIZE; jj += N_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 6
          for (int kk = kkk; kk < kkk + K_L2_CB_SIZE; kk += K_L1_CB_SIZE) {
            for (int jj = jjj; jj < jjj + N_L2_CB_SIZE; jj += N_L1_CB_SIZE) {
              for (int ii = iii; ii < iii + M_L2_CB_SIZE; ii += M_L1_CB_SIZE) {
#endif

                // dot product
                for (int i = ii; i < ii + M_L1_CB_SIZE; ++i) {
                  for (int j = jj; j < jj + N_L1_CB_SIZE; ++j) {
                    for (int k = kk; k < kk + K_L1_CB_SIZE; ++k) {
                      sum += A(i, k) * B(k, j);
                    }
                    C(i, j) += sum;
                    sum = 0.f;
                  }
                } // end dot product
                  // for (int i = 0; i < M; ++i) {
                  //   for (int j = 0; j < N; ++j) {
                  //     for (int k = 0; k < K; ++k) {
                  //       sum += A(i, k) * B(k, j);
                  //     }
                  //     C(i, j) += sum;
                  //     sum = 0.f;
                  //   }
                  // } // end dot product
              }
            }
          } // end L1
        }
      }
    } // end L2
  }
};

class GemmTBBv4 : public Benchmarkable {
private:
  float *a;
  float *b;
  float *c;

public:
  GemmTBBv4(float (*initA)(int, int), float (*initB)(int, int)) {
    a = new float[M * K];
    b = new float[K * N];
    c = new float[M * N];

    // Initialize A
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < K; j++) {
        A(i, j) = initA(i, j);
      }
    }

    // Initialize B
    for (int i = 0; i < K; i++) {
      for (int j = 0; j < N; j++) {
        B(i, j) = initB(i, j);
      }
    }

    // Initialize C
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < N; j++) {
        C(i, j) = 0.f;
      }
    }
  }

  ~GemmTBBv4() {
    delete[] a;
    delete[] b;
    delete[] c;
  }

  void run() {
    tbb::parallel_for(
        tbb::blocked_range2d<int>(0, M, M_L2_CB_SIZE, 0, N, N_L2_CB_SIZE),
        Multiply(a, b, c), tbb::simple_partitioner());
    // Multiply(a, b, c)();
  }

  float getResult(int i, int j) { return C(i, j); }
};
}
