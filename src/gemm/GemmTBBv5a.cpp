#include "../Benchmarkable.cpp"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include <stdio.h>

// This version does:
// - Cache Blocking for L1, L2 and L3 Cache

// Best Configurations (ATF):
// restrict&pragma  | without         | with

// Machine          sw03  sw03  m01   | sw03  m01
// Compiler         gcc   icc   gcc   | icc   icc
// Size             1024  1024  1024  | 1024  1024
// Cost (ms)        23    97    13    | 39    25
//
// M_L3_CB_SIZE     128   256   4     | 32    4
// M_L2_CB_SIZE     8     64    1     | 32    4
// M_L1_CB_SIZE     1     2     1     | 1     4
//
// N_L3_CB_SIZE     1024  1024  1024  | 16    1024
// N_L2_CB_SIZE     1024  1024  1024  | 4     1024
// N_L1_CB_SIZE     1024  1024  1024  | 4     1024
//
// K_L3_CB_SIZE     256   1024  16    | 512   128
// K_L2_CB_SIZE     4     128   4     | 32    4
// K_L1_CB_SIZE     4     4     4     | 16    4
//
// L3_LOOP_ORDER    6     1     5     | 1     4
// L2_LOOP_ORDER    2     6     5     | 5     1
// L1_LOOP_ORDER    5     6     5     | 6     4

// m01: (ATF 1h, #pragma vector always, without optimization flags)
// best configuration: [ K_L1_CB_SIZE = 32 ; K_L2_CB_SIZE = 64 ; K_L3_CB_SIZE =
// 64 ; L1_LOOP_ORDER = 6 ; L2_LOOP_ORDER = 2 ; L3_LOOP_ORDER = 6 ; M_L1_CB_SIZE
// = 2 ; M_L2_CB_SIZE = 64 ; M_L3_CB_SIZE = 64 ; N_L1_CB_SIZE = 16 ;
// N_L2_CB_SIZE = 64 ; N_L3_CB_SIZE = 64 ] with cost: 17

// m01: (ATF 1h, #pragma vector always, with optimization flags)
// best configuration: [ K_L1_CB_SIZE = 64 ; K_L2_CB_SIZE = 64 ; K_L3_CB_SIZE =
// 512 ; L1_LOOP_ORDER = 4 ; L2_LOOP_ORDER = 5 ; L3_LOOP_ORDER = 6 ;
// M_L1_CB_SIZE = 8 ; M_L2_CB_SIZE = 16 ; M_L3_CB_SIZE = 32 ; N_L1_CB_SIZE = 16
// ; N_L2_CB_SIZE = 16 ; N_L3_CB_SIZE = 32 ] with cost: 12

// sw03: (ATF 1h, #pragma vector always, with optimization flags)
// best configuration: [ K_L1_CB_SIZE = 64 ; K_L2_CB_SIZE = 128 ; K_L3_CB_SIZE =
// 1024 ; L1_LOOP_ORDER = 6 ; L2_LOOP_ORDER = 1 ; L3_LOOP_ORDER = 3 ;
// M_L1_CB_SIZE = 1 ; M_L2_CB_SIZE = 32 ; M_L3_CB_SIZE = 32 ; N_L1_CB_SIZE = 16
// ; N_L2_CB_SIZE = 512 ; N_L3_CB_SIZE = 1024 ] with cost: 20

#define M 1024
#define N 1024
#define K 1024

#ifndef M_L3_CB_SIZE
#define M_L3_CB_SIZE 64 // constraint: divides( M )
#endif

#ifndef M_L2_CB_SIZE
#define M_L2_CB_SIZE 32 // constraint: divides( M_L3_CB_SIZE )
#endif

#ifndef M_L1_CB_SIZE
#define M_L1_CB_SIZE 16 // constraint: divides( M_L2_CB_SIZE )
#endif

#ifndef N_L3_CB_SIZE
#define N_L3_CB_SIZE 64 // constraint: divides( N )
#endif

#ifndef N_L2_CB_SIZE
#define N_L2_CB_SIZE 32 // constraint: divides( N_L3_CB_SIZE )
#endif

#ifndef N_L1_CB_SIZE
#define N_L1_CB_SIZE 16 // constraint: divides( N_L2_CB_SIZE )
#endif

#ifndef K_L3_CB_SIZE
#define K_L3_CB_SIZE 64 // constraint: divides( K )
#endif

#ifndef K_L2_CB_SIZE
#define K_L2_CB_SIZE 32 // constraint: divides( K_L3_CB_SIZE )
#endif

#ifndef K_L1_CB_SIZE
#define K_L1_CB_SIZE 16 // constraint: divides( K_L2_CB_SIZE )
#endif

#ifndef L3_LOOP_ORDER
#define L3_LOOP_ORDER 1 // range: {1,...,6}
#endif

#ifndef L2_LOOP_ORDER
#define L2_LOOP_ORDER 1 // range: {1,...,6}
#endif

#ifndef L1_LOOP_ORDER
#define L1_LOOP_ORDER 1 // range: {1,...,6}
#endif

namespace gemmTBBv5a {

#define A(i, j) a[(i)*K + (j)]
#define B(i, j) b[(i)*N + (j)]
#define C(i, j) c[(i)*N + (j)]

class Multiply {
private:
  float *__restrict__ a;
  float *__restrict__ b;
  float *__restrict__ c;

public:
  Multiply(float *a, float *b, float *c) : a(a), b(b), c(c){};

  void operator()(const tbb::blocked_range2d<int> &range) const {

    float sum = 0.f;
    auto rb = range.rows().begin() * M_L3_CB_SIZE;
    auto re = range.rows().end() * M_L3_CB_SIZE;
    auto cb = range.cols().begin() * N_L3_CB_SIZE;
    auto ce = range.cols().end() * N_L3_CB_SIZE;

    int k_end = ((int)K / K_L3_CB_SIZE) * K_L3_CB_SIZE;

// L3
#if L3_LOOP_ORDER == 1
    for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
      for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
        for (int kkkk = 0; kkkk < k_end; kkkk += K_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 2
    for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
      for (int kkkk = 0; kkkk < k_end; kkkk += K_L3_CB_SIZE) {
        for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 3
    for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
      for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
        for (int kkkk = 0; kkkk < k_end; kkkk += K_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 4
    for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
      for (int kkkk = 0; kkkk < k_end; kkkk += K_L3_CB_SIZE) {
        for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 5
    for (int kkkk = 0; kkkk < k_end; kkkk += K_L3_CB_SIZE) {
      for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
        for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 6
    for (int kkkk = 0; kkkk < k_end; kkkk += K_L3_CB_SIZE) {
      for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
        for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
#endif

// L2
#if L2_LOOP_ORDER == 1
          for (int iii = iiii; iii < iiii + M_L3_CB_SIZE; iii += M_L2_CB_SIZE) {
            for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE;
                 jjj += N_L2_CB_SIZE) {
              for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE;
                   kkk += K_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 2
          for (int iii = iiii; iii < iiii + M_L3_CB_SIZE; iii += M_L2_CB_SIZE) {
            for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE;
                 kkk += K_L2_CB_SIZE) {
              for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE;
                   jjj += N_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 3
          for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE; jjj += N_L2_CB_SIZE) {
            for (int iii = iiii; iii < iiii + M_L3_CB_SIZE;
                 iii += M_L2_CB_SIZE) {
              for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE;
                   kkk += K_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 4
          for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE; jjj += N_L2_CB_SIZE) {
            for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE;
                 kkk += K_L2_CB_SIZE) {
              for (int iii = iiii; iii < iiii + M_L3_CB_SIZE;
                   iii += M_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 5
          for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE; kkk += K_L2_CB_SIZE) {
            for (int iii = iiii; iii < iiii + M_L3_CB_SIZE;
                 iii += M_L2_CB_SIZE) {
              for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE;
                   jjj += N_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 6
          for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE; kkk += K_L2_CB_SIZE) {
            for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE;
                 jjj += N_L2_CB_SIZE) {
              for (int iii = iiii; iii < iiii + M_L3_CB_SIZE;
                   iii += M_L2_CB_SIZE) {
#endif

// L1
#if L1_LOOP_ORDER == 1
                for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                     ii += M_L1_CB_SIZE) {
                  for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                       jj += N_L1_CB_SIZE) {
                    for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                         kk += K_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 2
                for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                     ii += M_L1_CB_SIZE) {
                  for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                       kk += K_L1_CB_SIZE) {
                    for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                         jj += N_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 3
                for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                     jj += N_L1_CB_SIZE) {
                  for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                       ii += M_L1_CB_SIZE) {
                    for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                         kk += K_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 4
                for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                     jj += N_L1_CB_SIZE) {
                  for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                       kk += K_L1_CB_SIZE) {
                    for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                         ii += M_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 5
                for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                     kk += K_L1_CB_SIZE) {
                  for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                       ii += M_L1_CB_SIZE) {
                    for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                         jj += N_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 6
                for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                     kk += K_L1_CB_SIZE) {
                  for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                       jj += N_L1_CB_SIZE) {
                    for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                         ii += M_L1_CB_SIZE) {
#endif

                      // dot product
                      for (int i = ii; i < ii + M_L1_CB_SIZE; ++i) {
                        for (int j = jj; j < jj + N_L1_CB_SIZE; ++j) {
#pragma vector always
                          for (int k = kk; k < kk + K_L1_CB_SIZE; ++k) {
                            sum += A(i, k) * B(k, j);
                          }
                          C(i, j) += sum;
                          sum = 0.f;
                        }
                      } // end dot product
                    }
                  }
                } // end L1
              }
            }
          } // end L2
        }
      }
    } // end L3
  }
};

class GemmTBBv5a : public Benchmarkable {
private:
  float *a;
  float *b;
  float *c;

public:
  GemmTBBv5a(float (*initA)(int, int), float (*initB)(int, int)) {
    a = new float[M * K];
    b = new float[K * N];
    c = new float[M * N];

    // Initialize A
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < K; j++) {
        A(i, j) = initA(i, j);
      }
    }

    // Initialize B
    for (int i = 0; i < K; i++) {
      for (int j = 0; j < N; j++) {
        B(i, j) = initB(i, j);
      }
    }

    // Initialize C
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < N; j++) {
        C(i, j) = 0.f;
      }
    }
  }

  ~GemmTBBv5a() {
    delete[] a;
    delete[] b;
    delete[] c;
  }

  void matmul_naive(int m, int n, int k, const float *a, const float *b,
                    float *c) const {
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        for (int p = 0; p < k; p++) {
          // printf("A(%i, %i) * B(%i, %i) = %f * %f = %f\n", i + 32, p, p, j +
          // 32,
          // A(i, p), B(p, j), A(i, p) * B(p, j));
          C(i, j) += A(i, p) * B(p, j);
        }
      }
    }
  }

  void run() {
    int m = ((int)M / M_L3_CB_SIZE) * M_L3_CB_SIZE;
    int n = ((int)N / N_L3_CB_SIZE) * N_L3_CB_SIZE;
    int k = ((int)K / K_L3_CB_SIZE) * K_L3_CB_SIZE;

    if (M_L3_CB_SIZE <= M && N_L3_CB_SIZE <= N && K_L3_CB_SIZE <= K) {
      tbb::parallel_for(tbb::blocked_range2d<int>(0, m / M_L3_CB_SIZE, 1, 0,
                                                  n / N_L3_CB_SIZE, 1),
                        Multiply(a, b, c), tbb::simple_partitioner());
    }

    size_t rest_m = M - m;
    size_t rest_n = N - n;
    size_t rest_k = K - k;

    if (rest_m > 0) {
      matmul_naive(rest_m, n, k, &A(m, 0), &B(0, 0), &C(m, 0));
    }

    if (rest_n > 0) {
      matmul_naive(m, rest_n, k, &A(0, 0), &B(0, n), &C(0, n));
    }

    if (rest_k > 0) {
      matmul_naive(m, n, rest_k, &A(0, k), &B(k, 0), &C(0, 0));
    }

    if (rest_m > 0 && rest_n > 0) {
      matmul_naive(rest_m, rest_n, k, &A(m, 0), &B(0, n), &C(m, n));
    }

    if (rest_m > 0 && rest_k > 0) {
      matmul_naive(rest_m, n, rest_k, &A(m, k), &B(k, 0), &C(m, 0));
    }

    if (rest_n > 0 && rest_k > 0) {
      matmul_naive(m, rest_n, rest_k, &A(0, k), &B(k, n), &C(0, n));
    }

    if (rest_m > 0 && rest_n > 0 && rest_k > 0) {
      matmul_naive(rest_m, rest_n, rest_k, &A(m, k), &B(k, n), &C(m, n));
    }
  }

  float getResult(int i, int j) { return C(i, j); }
};
}
