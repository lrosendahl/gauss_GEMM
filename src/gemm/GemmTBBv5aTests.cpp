#include "GemmMKL.cpp"
#include "GemmTBBv5a.cpp"
#include <algorithm>
#include <cmath>

#define EPSILON (float)0.001

// Using the #define's from GemmTBBv5a
// #define M 1024
// #define N 1024
// #define K 1024

// Used to initialize matrices A and B
float initValues(int i, int j) { return i * j + 1; }

int main() {
  gemmTBBv5a::GemmTBBv5a gemmTBB(initValues, initValues);
  GemmMKL gemmMKL(M, K, N, initValues, initValues);
  gemmTBB.run();
  gemmMKL.run();

  bool failed = false;

  printf("Testing matrices of size %ix%i * %ix%i:\n", M, K, K, N);

  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      float a = gemmTBB.getResult(i, j);
      float b = gemmMKL.getResult(i, j);
      if (fabs(a - b) / std::max(a, b) > EPSILON) {
        printf("Failed at %i, %i: %f != %f\n", i, j, a, b);
        failed = true;
      }
    }
  }

  if (!failed)
    printf("Success.\n");

  return 0;
}
