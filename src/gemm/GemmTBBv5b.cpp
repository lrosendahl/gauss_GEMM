#include "../Benchmarkable.cpp"
#include "libjit_defs.h"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include <stdio.h>

// This version does:
// - Cache Blocking for L1, L2 and L3 Cache

#define M 1024
#define N 1024
#define K 1024

#ifndef M_L3_CB_SIZE
#define M_L3_CB_SIZE 256 // constraint: divides( M )
#endif

#ifndef M_L2_CB_SIZE
#define M_L2_CB_SIZE 64 // constraint: divides( M_L3_CB_SIZE )
#endif

#ifndef M_L1_CB_SIZE
#define M_L1_CB_SIZE 32 // constraint: divides( M_L2_CB_SIZE )
#endif

#ifndef N_L3_CB_SIZE
#define N_L3_CB_SIZE 64 // constraint: divides( N )
#endif

#ifndef N_L2_CB_SIZE
#define N_L2_CB_SIZE 16 // constraint: divides( N_L3_CB_SIZE )
#endif

#ifndef N_L1_CB_SIZE
#define N_L1_CB_SIZE 8 // constraint: divides( N_L2_CB_SIZE )
#endif

#ifndef K_L3_CB_SIZE
#define K_L3_CB_SIZE 256 // constraint: divides( K )
#endif

#ifndef K_L2_CB_SIZE
#define K_L2_CB_SIZE 64 // constraint: divides( K_L3_CB_SIZE )
#endif

#ifndef K_L1_CB_SIZE
#define K_L1_CB_SIZE 8 // constraint: divides( K_L2_CB_SIZE )
#endif

#ifndef L3_LOOP_ORDER
#define L3_LOOP_ORDER 1 // range: {1,...,6}
#endif

#ifndef L2_LOOP_ORDER
#define L2_LOOP_ORDER 1 // range: {1,...,6}
#endif

#ifndef L1_LOOP_ORDER
#define L1_LOOP_ORDER 1 // range: {1,...,6}
#endif

#ifndef REGS_A
#define REGS_A 4
#endif

#ifndef REGS_B
#define REGS_B 3
#endif

#ifndef MR
#define MR 32 // MR = REGS_A * 8 for AVX (8 floats per SIMD register)
#endif

#ifndef NR
#define NR 3 // NR = REGS_B
#endif

namespace gemmTBBv5b {

#define A(i, j) a[(i)*K + (j)]
#define B(i, j) b[(i)*N + (j)]
#define C(i, j) c[(i)*N + (j)]

class Multiply {
private:
  float *a;
  float *b;
  float *c;

public:
  Multiply(float *a, float *b, float *c) : a(a), b(b), c(c){};

  // Helper for the remaining parts of the matrices in the inner kernel
  void matmul_naive(int m, int n, int k, const float *a, const float *b,
                    float *c) const {
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        for (int p = 0; p < k; p++) {
          C(i, j) += A(i, p) * B(p, j);
        }
      }
    }
  }

  // Vectorized dot product
  void dot(const float *a, const float *b, float *c) const {
    float8 csum[REGS_A][REGS_B] = {{0.0}};

    for (size_t p = 0; p < K_L1_CB_SIZE; p++) {
      // Perform the dot product
      for (size_t ai = 0; ai < REGS_A; ai++) {
        float8 aa = LoaduFloat8(&A(ai * 8, p));
        for (size_t bi = 0; bi < REGS_B; bi++) {
          float8 bb = BroadcastFloat8(B(p, bi));
          csum[ai][bi] += aa * bb;
        }
      }
    }

    // Accumulate the results into C
    for (size_t bi = 0; bi < REGS_B; bi++) {
      for (size_t ai = 0; ai < REGS_A; ai++) {
        AdduFloat8(&C(ai * 8, bi), csum[ai][bi]);
      }
    }

    // float sum = 0.f;
    // // TODO: Loop order
    // for (int i = 0; i < MR; i++) {
    //   for (int j = 0; j < NR; j++) {
    //     for (int k = 0; k < K_L1_CB_SIZE; ++k) {
    //       sum += A(i, k) * B(k, j);
    //     }
    //     C(i, j) += sum;
    //     sum = 0.f;
    //   }
    // }
  }

  // Inner kernel
  void inner(const float *a, const float *b, float *c) const {
    // TODO: Loop order
    for (int i_reg = 0; i_reg < M_L1_CB_SIZE - MR + 1; i_reg += MR) {
      for (int j_reg = 0; j_reg < N_L1_CB_SIZE - NR + 1; j_reg += NR) {
        dot(&A(i_reg, 0), &B(0, j_reg), &C(i_reg, j_reg));
      }
    }

    // If M_L1_CB_SIZE doesn't divide MR or N_L1_CB_SIZE doesn't divide NR
    // calculate the remaining parts of the matrix using a naive
    // implementation
    size_t i = (M_L1_CB_SIZE / MR) * MR;
    size_t j = (N_L1_CB_SIZE / NR) * NR;

    if (i < M_L1_CB_SIZE) {
      matmul_naive(M_L1_CB_SIZE - i, j, K_L1_CB_SIZE, &A(i, 0), &B(0, 0),
                   &C(i, 0));
    }

    if (j < N_L1_CB_SIZE) {
      matmul_naive(i, N_L1_CB_SIZE - j, K_L1_CB_SIZE, &A(0, 0), &B(0, j),
                   &C(0, j));
    }

    if (i < M_L1_CB_SIZE && j < N_L1_CB_SIZE) {
      matmul_naive(M_L1_CB_SIZE - i, N_L1_CB_SIZE - j, K_L1_CB_SIZE, &A(i, 0),
                   &B(0, j), &C(i, j));
    }
  }

  void operator()(const tbb::blocked_range2d<int> &range) const {

    auto rb = range.rows().begin();
    auto re = range.rows().end();
    auto cb = range.cols().begin();
    auto ce = range.cols().end();

// L3
#if L3_LOOP_ORDER == 1
    for (int i_L3 = rb; i_L3 < re; i_L3 += M_L3_CB_SIZE) {
      for (int j_L3 = cb; j_L3 < ce; j_L3 += N_L3_CB_SIZE) {
        for (int k_L3 = 0; k_L3 < K; k_L3 += K_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 2
    for (int i_L3 = rb; i_L3 < re; i_L3 += M_L3_CB_SIZE) {
      for (int k_L3 = 0; k_L3 < K; k_L3 += K_L3_CB_SIZE) {
        for (int j_L3 = cb; j_L3 < ce; j_L3 += N_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 3
    for (int j_L3 = cb; j_L3 < ce; j_L3 += N_L3_CB_SIZE) {
      for (int i_L3 = rb; i_L3 < re; i_L3 += M_L3_CB_SIZE) {
        for (int k_L3 = 0; k_L3 < K; k_L3 += K_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 4
    for (int j_L3 = cb; j_L3 < ce; j_L3 += N_L3_CB_SIZE) {
      for (int k_L3 = 0; k_L3 < K; k_L3 += K_L3_CB_SIZE) {
        for (int i_L3 = rb; i_L3 < re; i_L3 += M_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 5
    for (int k_L3 = 0; k_L3 < K; k_L3 += K_L3_CB_SIZE) {
      for (int i_L3 = rb; i_L3 < re; i_L3 += M_L3_CB_SIZE) {
        for (int j_L3 = cb; j_L3 < ce; j_L3 += N_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 6
    for (int k_L3 = 0; k_L3 < K; k_L3 += K_L3_CB_SIZE) {
      for (int j_L3 = cb; j_L3 < ce; j_L3 += N_L3_CB_SIZE) {
        for (int i_L3 = rb; i_L3 < re; i_L3 += M_L3_CB_SIZE) {
#endif

// L2
#if L2_LOOP_ORDER == 1
          for (int i_L2 = i_L3; i_L2 < i_L3 + M_L3_CB_SIZE;
               i_L2 += M_L2_CB_SIZE) {
            for (int j_L2 = j_L3; j_L2 < j_L3 + N_L3_CB_SIZE;
                 j_L2 += N_L2_CB_SIZE) {
              for (int k_L2 = k_L3; k_L2 < k_L3 + K_L3_CB_SIZE;
                   k_L2 += K_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 2
          for (int i_L2 = i_L3; i_L2 < i_L3 + M_L3_CB_SIZE;
               i_L2 += M_L2_CB_SIZE) {
            for (int k_L2 = k_L3; k_L2 < k_L3 + K_L3_CB_SIZE;
                 k_L2 += K_L2_CB_SIZE) {
              for (int j_L2 = j_L3; j_L2 < j_L3 + N_L3_CB_SIZE;
                   j_L2 += N_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 3
          for (int j_L2 = j_L3; j_L2 < j_L3 + N_L3_CB_SIZE;
               j_L2 += N_L2_CB_SIZE) {
            for (int i_L2 = i_L3; i_L2 < i_L3 + M_L3_CB_SIZE;
                 i_L2 += M_L2_CB_SIZE) {
              for (int k_L2 = k_L3; k_L2 < k_L3 + K_L3_CB_SIZE;
                   k_L2 += K_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 4
          for (int j_L2 = j_L3; j_L2 < j_L3 + N_L3_CB_SIZE;
               j_L2 += N_L2_CB_SIZE) {
            for (int k_L2 = k_L3; k_L2 < k_L3 + K_L3_CB_SIZE;
                 k_L2 += K_L2_CB_SIZE) {
              for (int i_L2 = i_L3; i_L2 < i_L3 + M_L3_CB_SIZE;
                   i_L2 += M_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 5
          for (int k_L2 = k_L3; k_L2 < k_L3 + K_L3_CB_SIZE;
               k_L2 += K_L2_CB_SIZE) {
            for (int i_L2 = i_L3; i_L2 < i_L3 + M_L3_CB_SIZE;
                 i_L2 += M_L2_CB_SIZE) {
              for (int j_L2 = j_L3; j_L2 < j_L3 + N_L3_CB_SIZE;
                   j_L2 += N_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 6
          for (int k_L2 = k_L3; k_L2 < k_L3 + K_L3_CB_SIZE;
               k_L2 += K_L2_CB_SIZE) {
            for (int j_L2 = j_L3; j_L2 < j_L3 + N_L3_CB_SIZE;
                 j_L2 += N_L2_CB_SIZE) {
              for (int i_L2 = i_L3; i_L2 < i_L3 + M_L3_CB_SIZE;
                   i_L2 += M_L2_CB_SIZE) {
#endif

// L1
#if L1_LOOP_ORDER == 1
                for (int i_L1 = i_L2; i_L1 < i_L2 + M_L2_CB_SIZE;
                     i_L1 += M_L1_CB_SIZE) {
                  for (int j_L1 = j_L2; j_L1 < j_L2 + N_L2_CB_SIZE;
                       j_L1 += N_L1_CB_SIZE) {
                    for (int k_L1 = k_L2; k_L1 < k_L2 + K_L2_CB_SIZE;
                         k_L1 += K_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 2
                for (int i_L1 = i_L2; i_L1 < i_L2 + M_L2_CB_SIZE;
                     i_L1 += M_L1_CB_SIZE) {
                  for (int k_L1 = k_L2; k_L1 < k_L2 + K_L2_CB_SIZE;
                       k_L1 += K_L1_CB_SIZE) {
                    for (int j_L1 = j_L2; j_L1 < j_L2 + N_L2_CB_SIZE;
                         j_L1 += N_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 3
                for (int j_L1 = j_L2; j_L1 < j_L2 + N_L2_CB_SIZE;
                     j_L1 += N_L1_CB_SIZE) {
                  for (int i_L1 = i_L2; i_L1 < i_L2 + M_L2_CB_SIZE;
                       i_L1 += M_L1_CB_SIZE) {
                    for (int k_L1 = k_L2; k_L1 < k_L2 + K_L2_CB_SIZE;
                         k_L1 += K_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 4
                for (int j_L1 = j_L2; j_L1 < j_L2 + N_L2_CB_SIZE;
                     j_L1 += N_L1_CB_SIZE) {
                  for (int k_L1 = k_L2; k_L1 < k_L2 + K_L2_CB_SIZE;
                       k_L1 += K_L1_CB_SIZE) {
                    for (int i_L1 = i_L2; i_L1 < i_L2 + M_L2_CB_SIZE;
                         i_L1 += M_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 5
                for (int k_L1 = k_L2; k_L1 < k_L2 + K_L2_CB_SIZE;
                     k_L1 += K_L1_CB_SIZE) {
                  for (int i_L1 = i_L2; i_L1 < i_L2 + M_L2_CB_SIZE;
                       i_L1 += M_L1_CB_SIZE) {
                    for (int j_L1 = j_L2; j_L1 < j_L2 + N_L2_CB_SIZE;
                         j_L1 += N_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 6
                for (int k_L1 = k_L2; k_L1 < k_L2 + K_L2_CB_SIZE;
                     k_L1 += K_L1_CB_SIZE) {
                  for (int j_L1 = j_L2; j_L1 < j_L2 + N_L2_CB_SIZE;
                       j_L1 += N_L1_CB_SIZE) {
                    for (int i_L1 = i_L2; i_L1 < i_L2 + M_L2_CB_SIZE;
                         i_L1 += M_L1_CB_SIZE) {
#endif
                      inner(&A(i_L1, k_L1), &B(k_L1, j_L1), &C(i_L1, j_L1));
                    }
                  }
                } // end L1
              }
            }
          } // end L2
        }
      }
    } // end L3
  }
};

class GemmTBBv5b : public Benchmarkable {
private:
  float *a;
  float *b;
  float *c;

public:
  GemmTBBv5b(float (*initA)(int, int), float (*initB)(int, int)) {
    a = new float[M * K];
    b = new float[K * N];
    c = new float[M * N];

    // Initialize A
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < K; j++) {
        A(i, j) = initA(i, j);
      }
    }

    // Initialize B
    for (int i = 0; i < K; i++) {
      for (int j = 0; j < N; j++) {
        B(i, j) = initB(i, j);
      }
    }

    // Initialize C
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < N; j++) {
        C(i, j) = 0.f;
      }
    }
  }

  ~GemmTBBv5b() {
    delete[] a;
    delete[] b;
    delete[] c;
  }

  void run() {
    tbb::parallel_for(
        tbb::blocked_range2d<int>(0, M, M_L3_CB_SIZE, 0, N, N_L3_CB_SIZE),
        Multiply(a, b, c), tbb::simple_partitioner());
  }

  float getResult(int i, int j) { return C(i, j); }
};

#undef A
#undef B
#undef C
}
