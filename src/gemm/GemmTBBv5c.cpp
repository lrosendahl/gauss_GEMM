#include "../Benchmarkable.cpp"
#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include <stdio.h>

// sw03 (g++):
// best configuration: [ K_L1_CB_SIZE = 2 ; K_L2_CB_SIZE = 32 ; K_L3_CB_SIZE =
// 128 ; L1_LOOP_ORDER = 1 ; L2_LOOP_ORDER = 3 ; L3_LOOP_ORDER = 6 ;
// M_L1_CB_SIZE = 2 ; M_L2_CB_SIZE = 2 ; M_L3_CB_SIZE = 1024 ; N_L1_CB_SIZE = 2
// ; N_L2_CB_SIZE = 4 ; N_L3_CB_SIZE = 8 ] with cost: 192

#define M 1024
#define N 1024
#define K 1024

#ifndef M_L3_CB_SIZE
#define M_L3_CB_SIZE 256 // constraint: divides( M )
#endif

#ifndef M_L2_CB_SIZE
#define M_L2_CB_SIZE 64 // constraint: divides( M_L3_CB_SIZE )
#endif

#ifndef M_L1_CB_SIZE
#define M_L1_CB_SIZE 32 // constraint: divides( M_L2_CB_SIZE )
#endif

#ifndef N_L3_CB_SIZE
#define N_L3_CB_SIZE 64 // constraint: divides( N )
#endif

#ifndef N_L2_CB_SIZE
#define N_L2_CB_SIZE 16 // constraint: divides( N_L3_CB_SIZE )
#endif

#ifndef N_L1_CB_SIZE
#define N_L1_CB_SIZE 8 // constraint: divides( N_L2_CB_SIZE )
#endif

#ifndef K_L3_CB_SIZE
#define K_L3_CB_SIZE 256 // constraint: divides( K )
#endif

#ifndef K_L2_CB_SIZE
#define K_L2_CB_SIZE 64 // constraint: divides( K_L3_CB_SIZE )
#endif

#ifndef K_L1_CB_SIZE
#define K_L1_CB_SIZE 8 // constraint: divides( K_L2_CB_SIZE )
#endif

#ifndef L3_LOOP_ORDER
#define L3_LOOP_ORDER 1 // range: {1,...,6}
#endif

#ifndef L2_LOOP_ORDER
#define L2_LOOP_ORDER 1 // range: {1,...,6}
#endif

#ifndef L1_LOOP_ORDER
#define L1_LOOP_ORDER 1 // range: {1,...,6}
#endif

namespace gemmTBBv5c {

#define A(i, j) a[(i)*K + (j)]
#define B(i, j) b[(i)*N + (j)]
#define B_T(i, j) b[(j)*K + (i)]
#define C(i, j) c[(i)*N + (j)]

class Multiply {
private:
  float *a;
  float *b;
  float *b_t;
  float *c;

public:
  Multiply(float *a, float *b, float *b_t, float *c)
      : a(a), b(b), b_t(b_t), c(c){};

  void operator()(const tbb::blocked_range2d<int> &range) const {

    float sum = 0.f;
    auto rb = range.rows().begin();
    auto re = range.rows().end();
    auto cb = range.cols().begin();
    auto ce = range.cols().end();

// L3
#if L3_LOOP_ORDER == 1
    for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
      for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
        for (int kkkk = 0; kkkk < K; kkkk += K_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 2
    for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
      for (int kkkk = 0; kkkk < K; kkkk += K_L3_CB_SIZE) {
        for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 3
    for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
      for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
        for (int kkkk = 0; kkkk < K; kkkk += K_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 4
    for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
      for (int kkkk = 0; kkkk < K; kkkk += K_L3_CB_SIZE) {
        for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 5
    for (int kkkk = 0; kkkk < K; kkkk += K_L3_CB_SIZE) {
      for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
        for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
#elif L3_LOOP_ORDER == 6
    for (int kkkk = 0; kkkk < K; kkkk += K_L3_CB_SIZE) {
      for (int jjjj = cb; jjjj < ce; jjjj += N_L3_CB_SIZE) {
        for (int iiii = rb; iiii < re; iiii += M_L3_CB_SIZE) {
#endif

// L2
#if L2_LOOP_ORDER == 1
          for (int iii = iiii; iii < iiii + M_L3_CB_SIZE; iii += M_L2_CB_SIZE) {
            for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE;
                 jjj += N_L2_CB_SIZE) {
              for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE;
                   kkk += K_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 2
          for (int iii = iiii; iii < iiii + M_L3_CB_SIZE; iii += M_L2_CB_SIZE) {
            for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE;
                 kkk += K_L2_CB_SIZE) {
              for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE;
                   jjj += N_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 3
          for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE; jjj += N_L2_CB_SIZE) {
            for (int iii = iiii; iii < iiii + M_L3_CB_SIZE;
                 iii += M_L2_CB_SIZE) {
              for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE;
                   kkk += K_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 4
          for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE; jjj += N_L2_CB_SIZE) {
            for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE;
                 kkk += K_L2_CB_SIZE) {
              for (int iii = iiii; iii < iiii + M_L3_CB_SIZE;
                   iii += M_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 5
          for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE; kkk += K_L2_CB_SIZE) {
            for (int iii = iiii; iii < iiii + M_L3_CB_SIZE;
                 iii += M_L2_CB_SIZE) {
              for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE;
                   jjj += N_L2_CB_SIZE) {
#elif L2_LOOP_ORDER == 6
          for (int kkk = kkkk; kkk < kkkk + K_L3_CB_SIZE; kkk += K_L2_CB_SIZE) {
            for (int jjj = jjjj; jjj < jjjj + N_L3_CB_SIZE;
                 jjj += N_L2_CB_SIZE) {
              for (int iii = iiii; iii < iiii + M_L3_CB_SIZE;
                   iii += M_L2_CB_SIZE) {
#endif

// L1
#if L1_LOOP_ORDER == 1
                for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                     ii += M_L1_CB_SIZE) {
                  for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                       jj += N_L1_CB_SIZE) {
                    for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                         kk += K_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 2
                for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                     ii += M_L1_CB_SIZE) {
                  for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                       kk += K_L1_CB_SIZE) {
                    for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                         jj += N_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 3
                for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                     jj += N_L1_CB_SIZE) {
                  for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                       ii += M_L1_CB_SIZE) {
                    for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                         kk += K_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 4
                for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                     jj += N_L1_CB_SIZE) {
                  for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                       kk += K_L1_CB_SIZE) {
                    for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                         ii += M_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 5
                for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                     kk += K_L1_CB_SIZE) {
                  for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                       ii += M_L1_CB_SIZE) {
                    for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                         jj += N_L1_CB_SIZE) {
#elif L1_LOOP_ORDER == 6
                for (int kk = kkk; kk < kkk + K_L2_CB_SIZE;
                     kk += K_L1_CB_SIZE) {
                  for (int jj = jjj; jj < jjj + N_L2_CB_SIZE;
                       jj += N_L1_CB_SIZE) {
                    for (int ii = iii; ii < iii + M_L2_CB_SIZE;
                         ii += M_L1_CB_SIZE) {
#endif

                      // dot product
                      for (int i = ii; i < ii + M_L1_CB_SIZE; i += 2) {
                        for (int j = jj; j < jj + N_L1_CB_SIZE; j += 2) {
                          float acc00 = 0, acc01 = 0, acc10 = 0, acc11 = 0;

                          for (int k = kk; k < kk + K_L1_CB_SIZE; ++k) {
                            acc00 += A(i, k) * B_T(k, j);
                            acc01 += A(i, k) * B_T(k, j + 1);
                            acc10 += A(i + 1, k) * B_T(k, j);
                            acc11 += A(i + 1, k) * B_T(k, j + 1);
                          }
                          C(i, j) += acc00;
                          C(i, j + 1) += acc01;
                          C(i + 1, j) += acc10;
                          C(i + 1, j + 1) += acc11;
                        }
                      } // end dot product
                    }
                  }
                } // end L1
              }
            }
          } // end L2
        }
      }
    } // end L3
  }
};

class formatB {
private:
  float *b;
  float *b_t;

public:
  formatB(float *b, float *b_t) : b(b), b_t(b_t){};

  void operator()(const tbb::blocked_range2d<size_t> &r) const {
    int rowStart = r.rows().begin();
    int rowEnd = r.rows().end();
    int colStart = r.cols().begin();
    int colEnd = r.cols().end();
    for (size_t i = rowStart; i != rowEnd; ++i) {
      for (size_t j = colStart; j != colEnd; ++j) {
        B_T(i, j) = B(i, j);
      }
    }
  }
};

class GemmTBBv5c : public Benchmarkable {
private:
  float *a;
  float *b;
  float *b_t;
  float *c;

public:
  GemmTBBv5c(float (*initA)(int, int), float (*initB)(int, int)) {
    a = new float[M * K];
    b = new float[K * N];
    b_t = new float[N * K];
    c = new float[M * N];

    // Initialize A
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < K; j++) {
        A(i, j) = initA(i, j);
      }
    }

    // Initialize B
    for (int i = 0; i < K; i++) {
      for (int j = 0; j < N; j++) {
        B(i, j) = initB(i, j);
      }
    }

    // Initialize C
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < N; j++) {
        C(i, j) = 0.f;
      }
    }
  }

  ~GemmTBBv5c() {
    delete[] a;
    delete[] b;
    delete[] b_t;
    delete[] c;
  }

  void run() {
    tbb::parallel_for(tbb::blocked_range2d<size_t>(0, K, 32, 0, N, 32),
                      formatB(b, b_t), tbb::simple_partitioner());

    tbb::parallel_for(
        tbb::blocked_range2d<int>(0, M, M_L3_CB_SIZE, 0, N, N_L3_CB_SIZE),
        Multiply(a, b, b_t, c), tbb::simple_partitioner());
  }

  float getResult(int i, int j) { return C(i, j); }
};
}
