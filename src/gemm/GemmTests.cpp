#define BOOST_TEST_DYN_LINK

#include "GemmMKL.cpp"
#include "GemmNaive.cpp"
#include "GemmTBBv1.cpp"
#include "GemmTBBv3.cpp"
#include "GemmTBBv4.cpp"
#include "GemmTBBv5a.cpp"
#include "GemmTBBv5b.cpp"
#include <boost/test/unit_test.hpp>

namespace gemm {

// MxK * KxN = MxN matrices

// These are for testing against the 4x16 matrix below
#define M_TEST 4
#define K_TEST 8
#define N_TEST 16

// These are for testing the newer (v4+) implementations
#define M 1024
#define N 1024
#define K 1024

#define EPSILON (float)0.001

// Used to initialize matrices A and B
float initValues(int i, int j) { return i * j + 1; }

// Expected output matrix (computed using NumPy)
// This is a 4x16 matrix
float expected[] = {
    8.0,    36.0,   64.0,   92.0,   120.0,  148.0,  176.0,  204.0,
    232.0,  260.0,  288.0,  316.0,  344.0,  372.0,  400.0,  428.0,
    36.0,   204.0,  372.0,  540.0,  708.0,  876.0,  1044.0, 1212.0,
    1380.0, 1548.0, 1716.0, 1884.0, 2052.0, 2220.0, 2388.0, 2556.0,
    64.0,   372.0,  680.0,  988.0,  1296.0, 1604.0, 1912.0, 2220.0,
    2528.0, 2836.0, 3144.0, 3452.0, 3760.0, 4068.0, 4376.0, 4684.0,
    92.0,   540.0,  988.0,  1436.0, 1884.0, 2332.0, 2780.0, 3228.0,
    3676.0, 4124.0, 4572.0, 5020.0, 5468.0, 5916.0, 6364.0, 6812.0};

// Test naive version
BOOST_AUTO_TEST_CASE(gemm_naive_test) {
  gemmNaive::GemmNaive gemmNaive(M_TEST, K_TEST, N_TEST, initValues,
                                 initValues);
  gemmNaive.run();

  for (int i = 0; i < M_TEST; i++) {
    for (int j = 0; j < N_TEST; j++) {
      BOOST_TEST(gemmNaive.getResult(i, j) == expected[i * N_TEST + j]);
    }
  }
}

// Test GEMM MKL version
BOOST_AUTO_TEST_CASE(gemm_mkl_test) {
  GemmMKL gemmMKL(M_TEST, K_TEST, N_TEST, initValues, initValues);
  gemmMKL.run();

  for (int i = 0; i < M_TEST; i++) {
    for (int j = 0; j < N_TEST; j++) {
      BOOST_TEST(gemmMKL.getResult(i, j) == expected[i * N_TEST + j]);
    }
  }
}

// Test GEMM TBB v1
BOOST_AUTO_TEST_CASE(gemm_tbb_v1_test) {
  GemmTBBv1 gemmTBBv1(M_TEST, K_TEST, N_TEST, initValues, initValues);
  gemmTBBv1.run();

  for (int i = 0; i < M_TEST; i++) {
    for (int j = 0; j < N_TEST; j++) {
      BOOST_TEST(gemmTBBv1.getResult(i, j) == expected[i * N_TEST + j]);
    }
  }
}

// Test GEMM TBB v3
BOOST_AUTO_TEST_CASE(gemm_tbb_v3_test) {
  gemmTBBv3::GemmTBBv3 gemmTBBv3(M_TEST, K_TEST, N_TEST, initValues,
                                 initValues);
  gemmTBBv3.run();

  for (int i = 0; i < M_TEST; i++) {
    for (int j = 0; j < N_TEST; j++) {
      BOOST_TEST(gemmTBBv3.getResult(i, j) == expected[i * N_TEST + j]);
    }
  }
}

// Test naive version against MKL
BOOST_AUTO_TEST_CASE(gemm_naive_vs_mkl_test) {
  gemmNaive::GemmNaive gemmNaive(64, 64, 64, initValues, initValues);
  GemmMKL gemmMKL(64, 64, 64, initValues, initValues);
  gemmNaive.run();
  gemmMKL.run();

  for (int i = 0; i < 64; i++) {
    for (int j = 0; j < 64; j++) {
      BOOST_TEST(gemmNaive.getResult(i, j) == gemmMKL.getResult(i, j),
                 boost::test_tools::tolerance(EPSILON));
    }
  }
}

// Test naive version against TBB v3
BOOST_AUTO_TEST_CASE(gemm_naive_vs_tbb_v3_test) {
  gemmNaive::GemmNaive gemmNaive(256, 256, 256, initValues, initValues);
  gemmTBBv3::GemmTBBv3 gemmTBBv3(256, 256, 256, initValues, initValues);
  gemmNaive.run();
  gemmTBBv3.run();

  for (int i = 0; i < 256; i++) {
    for (int j = 0; j < 256; j++) {
      BOOST_TEST(gemmNaive.getResult(i, j) == gemmTBBv3.getResult(i, j),
                 boost::test_tools::tolerance(EPSILON));
    }
  }
}

// Test TBB v3 against MKL
BOOST_AUTO_TEST_CASE(gemm_tbb_v3_vs_mkl_test) {
  gemmTBBv3::GemmTBBv3 gemmTBB(64, 64, 64, initValues, initValues);
  GemmMKL gemmMKL(64, 64, 64, initValues, initValues);
  gemmTBB.run();
  gemmMKL.run();

  for (int i = 0; i < 64; i++) {
    for (int j = 0; j < 64; j++) {
      BOOST_TEST(gemmTBB.getResult(i, j) == gemmMKL.getResult(i, j),
                 boost::test_tools::tolerance(EPSILON));
    }
  }
}

// Test TBB v4 against MKL
BOOST_AUTO_TEST_CASE(gemm_tbb_v4_vs_mkl_test) {
  gemmTBBv4::GemmTBBv4 gemmTBB(initValues, initValues);
  GemmMKL gemmMKL(M, K, N, initValues, initValues);
  gemmTBB.run();
  gemmMKL.run();

  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      BOOST_TEST(gemmTBB.getResult(i, j) == gemmMKL.getResult(i, j),
                 boost::test_tools::tolerance(EPSILON));
    }
  }
}

// Test TBB v5a against MKL
BOOST_AUTO_TEST_CASE(gemm_tbb_v5a_vs_mkl_test) {
  gemmTBBv5a::GemmTBBv5a gemmTBB(initValues, initValues);
  GemmMKL gemmMKL(M, K, N, initValues, initValues);
  gemmTBB.run();
  gemmMKL.run();

  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      BOOST_TEST(gemmTBB.getResult(i, j) == gemmMKL.getResult(i, j),
                 boost::test_tools::tolerance(EPSILON));
    }
  }
}

// Test TBB v5b against MKL
BOOST_AUTO_TEST_CASE(gemm_tbb_v5b_vs_mkl_test) {
  gemmTBBv5b::GemmTBBv5b gemmTBB(initValues, initValues);
  GemmMKL gemmMKL(M, K, N, initValues, initValues);
  gemmTBB.run();
  gemmMKL.run();

  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      BOOST_TEST(gemmTBB.getResult(i, j) == gemmMKL.getResult(i, j),
                 boost::test_tools::tolerance(EPSILON));
    }
  }
}
}
