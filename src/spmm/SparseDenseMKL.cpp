#include "../Benchmarkable.cpp"
#include "mkl.h"
#include <stdio.h>

class SparseDenseMKL : public Benchmarkable {
private:
  int numValues;
  int m;
  int n;
  int k;

  float *values;
  int *columns;
  int *pointerE;
  int *pointerB;

  sparse_matrix_t a;
  float *b;
  float *c;

  const float alpha = 1.0;
  const float beta = 0.0;

  struct matrix_descr descr;

public:
  SparseDenseMKL(int m, int k, int n, int numValues, float *values,
                 int *columns, int *pointerB, int *pointerE,
                 float (*initB)(int, int))
      : m(m), k(k), n(n), numValues(numValues), values(values),
        columns(columns), pointerB(pointerB), pointerE(pointerE) {

    descr.type = SPARSE_MATRIX_TYPE_GENERAL;
    // descr.mode = SPARSE_FILL_MODE_UPPER;
    // descr.diag = SPARSE_DIAG_NON_UNIT;

    mkl_sparse_s_create_csr(&a, SPARSE_INDEX_BASE_ZERO, m, k, pointerB,
                            pointerE, columns, values);
    b = (float *)mkl_malloc(k * n * sizeof(float), 64);
    c = (float *)mkl_malloc(m * n * sizeof(float), 64);

    if (b == NULL || c == NULL) {
      printf("ERROR: Failed to allocate memory for matrices. Aborting... \n");
      mkl_free(b);
      mkl_free(c);
    }

    for (int i = 0; i < k; i++) {
      for (int j = 0; j < n; j++) {
        b[i * n + j] = initB(i, j);
      }
    }

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        c[i * n + j] = 0;
      }
    }
  }

  ~SparseDenseMKL() {
    mkl_free(a);
    mkl_free(b);
    mkl_free(c);
  }

  void run() {
    // y := A * x + y
    // c := a * b + c
    mkl_sparse_s_mm(SPARSE_OPERATION_NON_TRANSPOSE, alpha, a, descr,
                    SPARSE_LAYOUT_ROW_MAJOR, b, n, n, beta, c, n);
  }

  float getResult(int i, int j) { return c[i * n + j]; }
};
