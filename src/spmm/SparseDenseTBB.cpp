#include "../Benchmarkable.cpp"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#ifndef RANGEX
#define RANGEX 32
#endif

namespace sparseDenseTBB {

class SparseDenseTBB : public Benchmarkable {
private:
  int numValues;
  int m;
  int n;
  int k;

  float *values;
  int *columns;
  int *pointerE;
  int *pointerB;
  float *b;
  float *c;

public:
  SparseDenseTBB(int m, int k, int n, int numValues, float *values,
                 int *columns, int *pointerB, int *pointerE,
                 float (*initB)(int, int))
      : m(m), k(k), n(n), numValues(numValues), values(values),
        columns(columns), pointerB(pointerB), pointerE(pointerE) {
    b = new float[k * n];
    c = new float[m * n];

    for (int i = 0; i < k; i++) {
      for (int j = 0; j < n; j++) {
        b[i * n + j] = initB(i, j);
      }
    }

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        c[i * n + j] = 0;
      }
    }
  }

  ~SparseDenseTBB() {
    delete[] b;
    delete[] c;
  }

  void run() {
    auto multiply = [=](const tbb::blocked_range<size_t> &r) {
      int start = r.begin();
      int end = r.end();
      for (size_t i = start; i != end; ++i) {
        for (int j = 0; j < n; j++) {
          float sum = 0;
          for (int l = pointerB[i]; l < pointerE[i]; l++) {
            sum += values[l] * b[columns[l] * n + j];
          }
          c[i * n + j] = sum;
        }
      }
    };

    tbb::parallel_for(tbb::blocked_range<size_t>(0, m, RANGEX),
                      multiply, tbb::simple_partitioner());
  }

  float getResult(int i, int j) { return c[i * n + j]; }
};
} // namespace sparseDenseTBB
