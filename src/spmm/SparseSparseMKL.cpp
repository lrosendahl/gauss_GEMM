#include "../Benchmarkable.cpp"
#include "mkl.h"
#include <stdio.h>

class SparseSparseMKL : public Benchmarkable {
private:
  int aNumValues;
  int bNumValues;
  int m;
  int n;
  int k;

  float *aValues;
  int *aColumns;
  int *aPointerE;
  int *aPointerB;

  float *bValues;
  int *bColumns;
  int *bPointerE;
  int *bPointerB;

  sparse_matrix_t a;
  sparse_matrix_t b;
  float *c;

public:
  SparseSparseMKL(int m, int k, int n, int aNumValues, float *aValues,
                  int *aColumns, int *aPointerB, int *aPointerE, int bNumValues,
                  float *bValues, int *bColumns, int *bPointerB, int *bPointerE)
      : m(m), k(k), n(n), aNumValues(aNumValues), aValues(aValues),
        aColumns(aColumns), aPointerB(aPointerB), aPointerE(aPointerE),
        bNumValues(bNumValues), bValues(bValues), bColumns(bColumns),
        bPointerB(bPointerB), bPointerE(bPointerE) {

    mkl_sparse_s_create_csr(&a, SPARSE_INDEX_BASE_ZERO, m, k, aPointerB,
                            aPointerE, aColumns, aValues);

    mkl_sparse_s_create_csr(&b, SPARSE_INDEX_BASE_ZERO, k, n, bPointerB,
                            bPointerE, bColumns, bValues);

    c = new float[m * n];

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        c[i * n + j] = 0;
      }
    }
  }

  ~SparseSparseMKL() { mkl_free(c); }

  void run() {
    mkl_sparse_s_spmmd(SPARSE_OPERATION_NON_TRANSPOSE, a, b,
                       SPARSE_LAYOUT_ROW_MAJOR, c, n);
  }

  float getResult(int i, int j) { return c[i * n + j]; }
};
