#include "../Benchmarkable.cpp"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#ifndef RANGEX
#define RANGEX 32
#endif

namespace sparseSparseTBB {

class SparseSparseTBB : public Benchmarkable {
private:
  int aNumValues;
  int bNumValues;
  int m;
  int n;
  int k;

  float *aValues;
  int *aColumns;
  int *aPointerE;
  int *aPointerB;

  float *bValues;
  int *bColumns;
  int *bPointerE;
  int *bPointerB;

  float *c;

public:
  SparseSparseTBB(int m, int k, int n, int aNumValues, float *aValues,
                  int *aColumns, int *aPointerB, int *aPointerE, int bNumValues,
                  float *bValues, int *bColumns, int *bPointerB, int *bPointerE)
      : m(m), k(k), n(n), aNumValues(aNumValues), aValues(aValues),
        aColumns(aColumns), aPointerB(aPointerB), aPointerE(aPointerE),
        bNumValues(bNumValues), bValues(bValues), bColumns(bColumns),
        bPointerB(bPointerB), bPointerE(bPointerE) {

    c = new float[m * n];

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        c[i * n + j] = 0;
      }
    }
  }

  ~SparseSparseTBB() { delete[] c; }

  void run() {

    auto multiply = [=](const tbb::blocked_range<size_t> &r) {
      int start = r.begin();
      int end = r.end();
      for (size_t i = start; i != end; ++i) {
        for (int l = aPointerB[i]; l < aPointerE[i]; l++) {
          int bRow = aColumns[l];
          for (int o = bPointerB[bRow]; o < bPointerE[bRow]; o++) {
            c[i * n + bColumns[o]] += aValues[l] * bValues[o];
          }
        }
      }
    };
    tbb::parallel_for(tbb::blocked_range<size_t>(0, m, RANGEX),
                      multiply, tbb::simple_partitioner());
  };

  float getResult(int i, int j) { return c[i * n + j]; }
};
} // namespace sparseSparseTBB
